// SPDX-License-Identifier: GPL-2.0-only

#ifndef RAMCHECK_H
#define RAMCHECK_H

#include "event_loop.h"

int ram_register(el_ctx *el, unsigned poll_interval);

#endif // RAMCHECK_H

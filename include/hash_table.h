// SPDX-License-Identifier: GPL-2.0-only

#ifndef HASH_TABLE
#define HASH_TABLE

// Hash table for file descriptors
// ===============================
//
// This hash table has only one type of key (int), and one type of value
// (void*).
//
// It works with arbitrary integers, *except -1*, which is used
// internally as a sentinel value to denote free slots.
//
// It also does not own the memory regions behind the pointers,
// and will never dereference the pointers themselves.
//
// ht_final() and ht_clear() can never fail.
// ht_clear() has no effect if called with a key that isn't set.
// ht_get() returns NULL if the key isn't currently set.
// ht_init() and ht_set() may fail to allocate memory.  If that happens,
// they have no effect and return -1.
//
// Never call ht_set() again after an allocation failure, it may invoke
// undefined behaviour (which might mean a critical vulnerability).

#include <stddef.h>

typedef struct ht_ctx
{
	int *keys;
	void **values;
	size_t nb_items;
	size_t buf_size;
} ht_ctx;

int ht_init(ht_ctx *table, size_t size);
void ht_final(ht_ctx *table);

void *ht_get(ht_ctx *table, int key);
int ht_set(ht_ctx *table, int key, void *value);
void ht_clear(ht_ctx *table, int key);

#endif // HASH_TABLE

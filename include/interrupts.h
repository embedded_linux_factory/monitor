// SPDX-License-Identifier: GPL-2.0-only

#ifndef INTERRUPTS_H
#define INTERRUPTS_H

#include "event_loop.h"

int interrupts_register(el_ctx *el, unsigned poll_interval);

#endif // INTERRUPTS_H

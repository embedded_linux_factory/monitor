// SPDX-License-Identifier: GPL-2.0-only

#ifndef PROCESS_H
#define PROCESS_H

#include "event_loop.h"

int process_register(el_ctx *el, unsigned poll_interval, bool thread_enabled);

#endif /* PROCESS_ */

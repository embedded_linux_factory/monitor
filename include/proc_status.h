// SPDX-License-Identifier: GPL-2.0-only

#ifndef PROC_STATUS
#define PROC_STATUS

#include <pthread.h>
#include <stdbool.h>

#include "event_loop.h"

/**
 * @brief Register the proc status event loop subsystem.
 * Send a Netlink message to the kernel to start listening
 * to process events.
 *
 * @param el the event loop already initialized context
 * @return int EXIT_SUCCESS on success, EXIT_FAILURE on failure
 */
int proc_status_register(el_ctx *el);

#endif // PROC_STATUS
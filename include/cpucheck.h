// SPDX-License-Identifier: GPL-2.0-only

#ifndef CPUCHECK_H
#define CPUCHECK_H

#include "event_loop.h"

int cpu_register(el_ctx *el, unsigned poll_interval);

#endif /* CPUCHECK_ */

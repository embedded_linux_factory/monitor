// SPDX-License-Identifier: GPL-2.0-only

#ifndef PERIODIC_TASK_H
#define PERIODIC_TASK_H

#include "event_loop.h"

typedef struct
{
	/** User defined task, called periodically.
	 *
	 * Leaving it NULL is undefined behaviour, and will almost
	 * certainly crash the program immediately.
	 */
	void (*task)(void *ctx);

	/** User defined task, called once the event loop ends.
	 *
	 * Typical usage is to free the @ctx user context.  Leaving it NULL
	 * is a noop (for tasks that don't need an explicit shutdown).
	 */
	void (*end)(void *ctx);

	/** User context given to @task and @end
	 *
	 * It must be allocated by the user before registration,
	 * and freed in @end.
	 */
	void *ctx;
} periodic_task;

/** Register periodic task
 *
 * @el      Event loop executing the task.
 * @task    User defined task.
 * @period  Execution period in seconds.
 */
int periodic_task_register(el_ctx *el, periodic_task task, unsigned period);

#endif // PERIODIC_TASK_H

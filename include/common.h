// SPDX-License-Identifier: GPL-2.0-only

/**
 * @file common.h
 *
 * @brief Common file of the project.
 */

#ifndef COMMON_
#define COMMON_

#include <stdbool.h>
#include <stdint.h>

enum LogConfig
{
	LOG_CONF_SYSLOG,
	LOG_CONF_JOURNALCTL
};

struct config_data_struct
{
	uint16_t t_maj;
	bool enable_acquisition;
};

#endif // COMMON_

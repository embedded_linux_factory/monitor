// SPDX-License-Identifier: GPL-2.0-only

#ifndef EVENT_LOOP_H
#define EVENT_LOOP_H

// Generic Event Loop
// ==================
//
// Provides a way for various subsystem to subscribe file descriptors,
// and be called back whenever something happens on that descriptor.
// This is intended to be part of an event driven system, and allows the
// handling of concurrent events in a single thread.
//
// Possible events include:
// - Network connections
// - Network reads & writes
// - Netlink socket events
// - Pipe events
// - Timer events (timer_fd_create(2))
//
//
// Performance considerations
// --------------------------
//
// We use epoll(7) under the hood, which scales better than poll(2).
// Overall, some Care has been taken to minimise system calls and avoid
// wasting CPU cycles, but we haven't gone all the way:
//
// - malloc(3) is called every time a new descriptor is added to the
//   watch list, and free(3) is called every time one is removed.  This
//   is deemed good enough, considering that adding a descriptor
//   systematically involves a system call (a problem that kqueue from
//   BSD can avoid, but sadly we're using Linux).
//
// - I (Loup) couldn't avoid using a hash table internally to add &
//   remove descriptors.  This is because the API relies solely on file
//   descriptors, which are decided from outside the API.  Giving
//   handles back to the users would have allowed us to avoid the hash
//   table and use a simpler and somewhat faster pool allocator instead,
//   but this would hurt usability.
//
// - When processing events however, allocations and hash tables are
//   entirely bypassed: epoll_ctl(2) lets us specify an arbitrary
//   pointer for the user data it gives back to us, it doesn't have to
//   be a file descriptor.  So instead of retrieving a descriptor and
//   looking up the hash table, we retrieve a pointer directly to the
//   callback data, and call it right away, with minimal overhead.
//
//   It is not quite perfect: since each piece of callback data is
//   allocated separately, memory locality may suffer somewhat, and we
//   may have a few more cache misses than the absolute best we can do.
//   We could do better with a dedicated allocator, but I (Loup) believe
//   this will never be a consideration: even if we push it, we'll
//   likely hit context switch problems & kernel overload well before
//   those cache misses are a problem.
//
// Overall, event processing is expected to be the comon case, so this
// should be good enough.  Whatever future optimisation we consider I
// (Loup) would advise that we still favour the reception of events even
// if we have to sacrifice the rest.
//
//
// Error handling
// --------------
//
// All functions that return an `int` may fail in one way or another.
// The error code may be converted into a string with el_strerror().
// Except for the `EL_ALLOC` error, errors means an epoll(7) call has
// failed, and the errno variable (in <errno.h>) is now set.
//
// Initialisation & shut down are done with el_init() and el_final().
// The user must allocate the context themselves, typically as a local
// variable in main().
//
// To add & remove file descriptors from the watch list, we use el_add()
// and el_del().  The arguments are:
// - cb    : The callback to be called every time the event is triggered.
// - cb_ctx: The callback context, passed back to the callback.
// - int fd: The file descriptor being watched
// - mask  : A set of flags describing which event to watch.
//           They all have a corresponding epoll(7) equivalent.
//
// el_run_once() waits for the first event to occur, or time out, then
// exits.  el_run() calls el_run_once() in a loop untill being asked to
// stop, or an error has occured.
//
// el_stop() request that the event loop stops.  Any pending events will
// still be processed, but el_run() will exit as soon as they all are.
// It is *not* thread safe.  It is intended instead to be called from a
// callback or a signal handler that executes on the same thread as
// el_run().
//
// el_set_timeout() sets the maximum time el_run_once() blocks.  By
// default it waits forever.  It can be called before running the loop,
// or inside a callback like el_stop().  Note that it doesn't make much
// sense to use it with el_run(), since a non-infinite timeout will just
// cause the loop to restart.
//
// el_running() is a query that asks whether the run loop is running,
// and has not been asked to stop yet.
//
// el_timed_out() is a query that asks whether the last run ended in a
// time out or not.  It is mostly intended to be used in conjunction
// with a user-written loop that wants to make sure the loop wakes up
// from time to time, for instance to guarantee that some code is
// executed and the program isn't just left hanging.
//
// Note though that el_timed_out() is not suitable for regularly
// scheduled events: if enough events occur the loop will never timeout.
// Consider adding a timerfd descriptor to the watch list instead, this
// will not only work, but also save you the trouble of POSIX timers and
// signal handling.

#include "hash_table.h"
#include <stdbool.h>

// Error codes
#define EL_OK		 0
#define EL_ALLOC	 1
#define EL_CREATE	 2
#define EL_ADD		 3
#define EL_DEL		 4
#define EL_WAIT		 5
#define EL_NB_ERRORS 6

const char *el_strerror(int error);

// Event types
#define EL_IN	 (1 << 0) // Read
#define EL_PRI	 (1 << 1) // Exceptional condition
#define EL_OUT	 (1 << 2) // Write
#define EL_ERR	 (1 << 3) // Error condition
#define EL_HUP	 (1 << 4) // End of connection      (always listened to)
#define EL_RDHUP (1 << 5) // End of read connection (since Linux 2.6.17)
#define EL_FINAL (1 << 6) // el_final() has been called

typedef struct el_ctx
{
	ht_ctx cb_data;
	int epfd;
	unsigned timeout_ms;
	bool running;
	bool timed_out;
} el_ctx;

typedef void (*el_cb)(void *ctx, int fd, unsigned mask);

int el_init(el_ctx *ctx);
int el_final(el_ctx *ctx);

int el_add(el_ctx *ctx, el_cb cb, void *cb_ctx, int fd, unsigned mask);
int el_del(el_ctx *ctx, int fd);

int el_run(el_ctx *ctx);
int el_run_once(el_ctx *ctx);

void el_stop(el_ctx *ctx);
void el_set_timeout(el_ctx *ctx, unsigned milliseconds);

bool el_running(const el_ctx *ctx);
bool el_timed_out(const el_ctx *ctx);

#endif // EVENT_LOOP_H

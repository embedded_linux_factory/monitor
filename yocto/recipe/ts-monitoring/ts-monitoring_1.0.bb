DESCRIPTION = "Technology & Strategy monitoring software module."

LICENSE = "CLOSED"
LIC_FILES_CHKSUM = ""

SRC_URI = "git://git@gitlab.com/embedded_linux_factory/monitor.git;protocol=ssh;branch=C-project-monitor"
inherit meson pkgconfig

REQUIRED_DISTRO_FEATURES += "systemd"

DEPENDS += " \
    cyaml \
    systemd \
"

RDEPENDS:${PN} += " \
    systemd \
    cyaml \
"

EXTRA_OECMAKE = ""
SRCREV = "39fc2d5ecd0df9b791c0addd1070d4b3ab6d6213"
PV = "1.0.0+git${SRCPV}"
S = "${WORKDIR}/git"

PARALLEL_MAKE = ""


MONITORING_CONFIG_FILE = "${S}/config/generic.yaml"

do_install:append() {
  mkdir -p ${D}/etc/ts-monitoring

  # install default config file
  install -m 0755 ${MONITORING_CONFIG_FILE} ${D}/etc/ts-monitoring
}


FILES:${PN} += " \
  ${bindir}/* \
  ${etcdir}/ts-monitoring \
"
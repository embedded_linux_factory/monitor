# ELF Monitor - Usage Guide

## Overview
The ELF Monitor is a tool designed to monitor system metrics such as CPU usage, interrupts, RAM, and processes. It logs these metrics at configurable intervals and outputs the data to the console or a specified logging system.

---

## 1. Installation

### Prerequisites
Before installing the ELF Monitor, ensure you have the following prerequisites installed:
- **Meson build system**: For building the project.
- **Ninja**: For building the project.
- **CMake**: For configuring the build system.
- **GCC**: A compatible C compiler to build the software.
 
   ```bash
   sudo apt update
   sudo apt install -y libsystemd-dev libyaml-dev libpthread-stubs0-dev meson ninja-build
   ```
### Steps to Install ELF Monitor

1. **Clone the Repository**:

   Clone the ELF Monitor repository from the official source:

   ```bash
   git clone https://gitlab.com/embedded_linux_factory/monitor
   cd monitor/
   ```

2. **Build the Software**:

   Use Meson to configure the build directory and Ninja to build the software:

   ```bash
   mkdir build_dir
   cd build_dir
   meson ..
   ninja
   ```

   This will compile the ELF Monitor and prepare the necessary binaries.

3. **Install the ELF Monitor** (Optional):

   If you wish to install the ELF Monitor system-wide, run the following:

   ```bash
   sudo ninja install
   ```

   This will install the binaries to the appropriate directories on your system, allowing you to run `monitord` from anywhere.

---

## 2. Running the Monitor

### Launching the Monitor
To start the ELF Monitor, execute the following command with your desired configuration file:

```bash
./src/monitord ../config/generic.yaml
```

This command runs the monitor in the background using `generic.yaml`, a configuration file that sets up which metrics to log (CPU, interrupts, RAM, processes) and their respective logging intervals.

---

## 3. Monitor Output

While the monitor is running, it generates log entries based on the configuration settings. These logs provide real-time data on:

- **CPU Usage**: Logs CPU data, e.g., Processor data saving.
- **Interrupt Counts**: Logs interrupt activity, e.g., [Interrupts] update interrupts data.
- **Memory Usage**: Logs the current RAM usage.
- **Processes**: Tracks specified process activities.

These logs are essential for monitoring system performance.

---

## 4. Adjusting the Configuration

To modify what is logged or how often, edit the `generic.yaml` configuration file. For example, to disable CPU logging, change the `enabled` value under the `cpu` section to `false`:

```yaml
cpu:
  enabled: false
```

This provides flexibility to tailor the monitoring behavior to your needs.

---

## 5. Stopping the Monitor

To stop the monitor, press `Ctrl+C` in the terminal where it's running. This gracefully terminates the process and stops logging activities.

---

## 6. Running in the Background

If you want to run the monitor in the background without occupying the terminal, use the following command:

```bash
./src/monitord ../config/generic.yaml &
```

This allows you to continue using the terminal for other tasks while the monitor runs in the background.

---

## 7. Running Tests

The ELF Monitor supports both unit tests for individual modules and integration tests for the entire system. 

### Running Unit Tests

Unit tests check the functionality of each individual module (e.g., CPU logging, RAM monitoring). To run the unit tests:

```bash
cd build_dir
meson test
```

The test results will indicate the status of each test:
- **OK**: Test passed successfully.
- **Fail**: Test encountered an error.
- **Skipped**: Test was not run (usually due to missing dependencies).
- **Timeout**: Test took too long and was aborted.

For detailed results, refer to `build_dir/meson-logs/testlog.txt`.

### Running Integration Tests

Integration tests ensure that the ELF Monitor functions correctly when all components are working together.

#### Steps:
1. Start the monitor with a known configuration file (e.g., `config/generic.yaml`):

   ```bash
   ./src/monitord ../config/generic.yaml
   ```

2. Verify that the terminal output correctly logs each metric (CPU, interrupts, RAM, processes).
   
3. You can also adjust the configuration file to test different scenarios, such as disabling certain metrics or adjusting the logging intervals.

---

## Conclusion

The ELF Monitor provides an efficient way to track system metrics, with flexible configuration options. By following the steps outlined above, you can easily install, start monitoring, adjust the logging settings, and run tests to ensure the software is functioning as expected.

--- 


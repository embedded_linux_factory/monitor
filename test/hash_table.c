// SPDX-License-Identifier: GPL-2.0-only

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

#include "hash_table.h"

// Pseudo-random 64 bit number, based on xorshift*
uint64_t rand64(void)
{
	static uint64_t random_state = 12345; // Any non-zero value
	random_state ^= random_state >> 12;
	random_state ^= random_state << 25;
	random_state ^= random_state >> 27;
	return random_state * 0x2545F4914F6CDD1D; // magic constant
}

#define NB_KEYS		  10	 // Number of keys (we may reuse the same)
#define NB_OPERATIONS (1000) // Number of operations
#define DELETE_EVERY  3		 // Deleting every x operation on average
#define NB_FLAG_WORDS ((NB_KEYS + 63) / 64)

static unsigned memory[NB_KEYS]; // Just so we have many valid pointers
static uint64_t flags[NB_FLAG_WORDS] = {0};

static bool flag_get(size_t i)
{
	return (flags[i / 64] & ((uint64_t)1 << (i % 64))) != 0;
}
static void flag_set(size_t i)
{
	flags[i / 64] |= ((uint64_t)1 << (i % 64));
}
static void flag_clear(size_t i)
{
	flags[i / 64] &= ~((uint64_t)1 << (i % 64));
}

int main(void)
{
	// Test normal hash table operations
	// (Lots of insertions & deletions)
	ht_ctx table;
	while (ht_init(&table, 1))
	{
	} // get around allocation failures

	for (size_t i = 0; i < NB_OPERATIONS; i++)
	{
		// Set or clear a random element
		size_t key = (size_t)rand64() % NB_KEYS;
		if (rand64() % DELETE_EVERY)
		{
			flag_clear(key);
			ht_clear(&table, key);
		}
		else
		{
			flag_set(key);
			while (ht_set(&table, key, memory + key))
			{
			} // get around alloc fails
		}

		// Check the hash table has registered everything correctly.
		for (size_t i = 0; i < NB_KEYS; i++)
		{
			unsigned *ptr = ht_get(&table, i);
			if ((!flag_get(i) && ptr != NULL) || (flag_get(i) && ptr != memory + i))
			{
				fprintf(stderr, "Hash table content corrupted\n");
				return -1;
			}
		}
	}

	ht_final(&table);

	// Make sure the internal sizes are to the correct power of 2
	// (Uses internal state, may break even if the API does not)
	size_t ask_for[] = {0, 1, 2, 3, 4, 5, 7, 8, 9, 15, 16, 17};
	size_t expect[] = {1, 1, 2, 4, 4, 8, 8, 8, 16, 16, 16, 32};
	size_t nb_sizes = sizeof(ask_for) / sizeof(size_t);
	for (size_t i = 0; i < nb_sizes; i++)
	{
		while (ht_init(&table, ask_for[i]))
		{
		} // get around allocation failures
		if (table.buf_size != expect[i])
		{
			fprintf(stderr, "Buf size mismatch\n");
			fprintf(stderr, "Asked for %zu\n", ask_for[i]);
			fprintf(stderr, "Expected  %zu\n", expect[i]);
			fprintf(stderr, "got       %zu\n", table.buf_size);
			return -1;
		}
	}

	return 0;
}

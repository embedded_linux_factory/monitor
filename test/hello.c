// SPDX-License-Identifier: GPL-2.0-only

#include <stdio.h>

int main(void)
{
	printf("Hello, test!\n");
	return 0; // Test succeeds
}

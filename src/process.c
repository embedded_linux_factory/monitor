// SPDX-License-Identifier: GPL-2.0-only

#include "process.h"
#include "periodic_task.h"
#include "tools.h"

#include <dirent.h>
#include <inttypes.h>
#include <logger/logger.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define TASK_COMM_LEN 100

// Process info, from /proc/*/stat. Check "man 5 proc" for details.
//
// Note(loup): Since the man pages documents scanf format specifiers, I
// had no choice but use the corresponding C types.  I would have
// preferred fixed widths, but this is more likely to be portable, most
// notably accross 32-bit and 64-bit systems.
typedef struct
{
	int pid;								  // %d
	char comm[TASK_COMM_LEN];				  // %s
	char state;								  // %c
	int ppid;								  // %d
	int pgrp;								  // %d
	int session;							  // %d
	int tty_nr;								  // %d
	int tpgid;								  // %d
	unsigned flags;							  // %u
	unsigned long minflt;					  // %lu
	unsigned long cminflt;					  // %lu
	unsigned long majflt;					  // %lu
	unsigned long cmajflt;					  // %lu
	unsigned long utime;					  // %lu
	unsigned long stime;					  // %lu
	long cutime;							  // %ld
	long cstime;							  // %ld
	long priority;							  // %ld
	long nice;								  // %ld
	long num_threads;						  // %ld
	long itrealvalue;						  // %ld
	unsigned long long starttime;			  // %llu
	unsigned long vsize;					  // %lu
	long rss;								  // %ld
	unsigned long rsslim;					  // %lu
	unsigned long startcode;				  // %lu  [PT]
	unsigned long endcode;					  // %lu  [PT]
	unsigned long startstack;				  // %lu  [PT]
	unsigned long kstkesp;					  // %lu  [PT]
	unsigned long kstkeip;					  // %lu  [PT]
	unsigned long signal;					  // %lu
	unsigned long blocked;					  // %lu
	unsigned long sigignore;				  // %lu
	unsigned long sigcatch;					  // %lu
	unsigned long wchan;					  // %lu   [PT]
	unsigned long nswap;					  // %lu
	unsigned long cnswap;					  // %lu
	int exit_signal;						  // %d    (since Linux 2.1.22)
	int processor;							  // %d    (since Linux 2.2.8)
	unsigned rt_priority;					  // %u    (since Linux 2.5.19)
	unsigned policy;						  // %u    (since Linux 2.5.19)
	unsigned long long delayacct_blkio_ticks; // %llu  (since Linux 2.6.18)
	unsigned long guest_time;				  // %lu   (since Linux 2.6.24)
	long cguest_time;						  // %ld   (since Linux 2.6.24)
	unsigned long start_data;				  // %lu   (since Linux 3.3)  [PT]
	unsigned long end_data;					  // %lu   (since Linux 3.3)  [PT]
	unsigned long start_brk;				  // %lu   (since Linux 3.3)  [PT]
	unsigned long arg_start;				  // %lu   (since Linux 3.5)  [PT]
	unsigned long arg_end;					  // %lu   (since Linux 3.5)  [PT]
	unsigned long env_start;				  // %lu   (since Linux 3.5)  [PT]
	unsigned long env_end;					  // %lu   (since Linux 3.5)  [PT]
	int exit_code;							  // %d    (since Linux 3.5)  [PT]
} proc_stat;
typedef struct
{
	logger_ctx log_ctx;
	bool is_thread_enabled;
} process_ctx;

static void log_thread_data(void *ctx, char thread_log_data[], char *tid_stat_file_name)
{

	process_ctx *proc_ctx = (process_ctx *)ctx;

	FILE *stat_file = fopen(tid_stat_file_name, "r");
	if (stat_file == NULL)
	{
		// No file probably means the thread is already gone
		// Not necessary to log the error ; threads are usually quickly runned
		return;
	}
	proc_stat stat_data;

	// Handle error
	int ret;
	if ((ret = fscanf(
			 stat_file,
			 "%d %s %c %d %d %d %d %d "
			 "%u %lu %lu %lu %lu %lu %lu %ld %ld %ld "
			 "%ld %ld %ld %llu %lu %ld %lu %lu %lu %lu "
			 "%lu %lu %lu %lu %lu %lu %lu %lu %lu %d %d %u %u "
			 "%llu %lu %ld %lu %lu %lu %lu %lu %lu %lu %d [^\n]",
			 &stat_data.pid, stat_data.comm, &stat_data.state, &stat_data.ppid, &stat_data.pgrp, &stat_data.session,
			 &stat_data.tty_nr, &stat_data.tpgid, &stat_data.flags, &stat_data.minflt, &stat_data.cminflt,
			 &stat_data.majflt, &stat_data.cmajflt, &stat_data.utime, &stat_data.stime, &stat_data.cutime,
			 &stat_data.cstime, &stat_data.priority, &stat_data.nice, &stat_data.num_threads, &stat_data.itrealvalue,
			 &stat_data.starttime, &stat_data.vsize, &stat_data.rss, &stat_data.rsslim, &stat_data.startcode,
			 &stat_data.endcode, &stat_data.startstack, &stat_data.kstkesp, &stat_data.kstkeip, &stat_data.signal,
			 &stat_data.blocked, &stat_data.sigignore, &stat_data.sigcatch, &stat_data.wchan, &stat_data.nswap,
			 &stat_data.cnswap, &stat_data.exit_signal, &stat_data.processor, &stat_data.rt_priority, &stat_data.policy,
			 &stat_data.delayacct_blkio_ticks, &stat_data.guest_time, &stat_data.cguest_time, &stat_data.start_data,
			 &stat_data.end_data, &stat_data.start_brk, &stat_data.arg_start, &stat_data.arg_end, &stat_data.env_start,
			 &stat_data.env_end, &stat_data.exit_code)) != 52)
	{
		// Thread has finished before reading
		fclose(stat_file);
		return;
	};

	// Synthesise thread data in one line
	sprintf(thread_log_data,
			"pid : %d \t"
			"comm : %s \t "
			"state : %c \t "
			"ppid : %d \t "
			"pgrp : %d \t "
			"session : %u \t "
			"utime : %lu \t "
			"stime : %lu \t "
			"num_threads : %ld \t "
			"starttime : %llu \t "
			"vsize : %lu \t "
			"rss : %lu \t "
			"rsslim : %lu \n",
			stat_data.pid, stat_data.comm, stat_data.state, stat_data.ppid, stat_data.pgrp, stat_data.session,
			stat_data.utime, stat_data.stime, stat_data.num_threads, stat_data.starttime, stat_data.vsize,
			stat_data.rss, stat_data.rsslim);

	if (ferror(stat_file))
	{
		// TODO: log error
	}
	if (fclose(stat_file))
	{
		// TODO: log error
	}

	logger_add_log(proc_ctx->log_ctx, thread_log_data);
	logger_send_log(proc_ctx->log_ctx);
}

static void log_process_data(void *ctx)
{
	process_ctx *proc_ctx = (process_ctx *)ctx;
	char process_log_data[1024 * 5] = "";

	// Try to open /proc directory (should always succeed)
	DIR *d = opendir("/proc");
	if (!d)
	{
		// TODO: log error
		return;
	}

	struct dirent *dir;
	while ((dir = readdir(d)) != NULL)
	{
		// Skip regular files
		if (dir->d_type != DT_DIR)
		{
			continue;
		}
		// Skip non-process (not all digits) directories
		const char *d_name = dir->d_name;
		while (*d_name >= '0' && *d_name <= '9')
		{
			d_name++;
		}
		if (*d_name != '\0')
		{
			continue;
		}

		// Read stats from /proc/*/stat
		char stat_file_name[32 + sizeof(dir->d_name)];
		sprintf(stat_file_name, "/proc/%s/stat", dir->d_name);
		FILE *stat_file = fopen(stat_file_name, "r");
		if (stat_file == NULL)
		{
			// No file probably means the process is already gone
			// TODO: log the error anyway
			continue;
		}
		proc_stat stat_data;

		// Handle error
		if (fscanf(stat_file,
				   "%d %s %c %d %d %d %d %d "
				   "%u %lu %lu %lu %lu %lu %lu %ld %ld %ld "
				   "%ld %ld %ld %llu %lu %ld %lu %lu %lu %lu "
				   "%lu %lu %lu %lu %lu %lu %lu %lu %lu %d %d %u %u "
				   "%llu %lu %ld %lu %lu %lu %lu %lu %lu %lu %d [^\n]",
				   &stat_data.pid, stat_data.comm, &stat_data.state, &stat_data.ppid, &stat_data.pgrp,
				   &stat_data.session, &stat_data.tty_nr, &stat_data.tpgid, &stat_data.flags, &stat_data.minflt,
				   &stat_data.cminflt, &stat_data.majflt, &stat_data.cmajflt, &stat_data.utime, &stat_data.stime,
				   &stat_data.cutime, &stat_data.cstime, &stat_data.priority, &stat_data.nice, &stat_data.num_threads,
				   &stat_data.itrealvalue, &stat_data.starttime, &stat_data.vsize, &stat_data.rss, &stat_data.rsslim,
				   &stat_data.startcode, &stat_data.endcode, &stat_data.startstack, &stat_data.kstkesp,
				   &stat_data.kstkeip, &stat_data.signal, &stat_data.blocked, &stat_data.sigignore, &stat_data.sigcatch,
				   &stat_data.wchan, &stat_data.nswap, &stat_data.cnswap, &stat_data.exit_signal, &stat_data.processor,
				   &stat_data.rt_priority, &stat_data.policy, &stat_data.delayacct_blkio_ticks, &stat_data.guest_time,
				   &stat_data.cguest_time, &stat_data.start_data, &stat_data.end_data, &stat_data.start_brk,
				   &stat_data.arg_start, &stat_data.arg_end, &stat_data.env_start, &stat_data.env_end,
				   &stat_data.exit_code) != 52)
		{
			logger_add_log(proc_ctx->log_ctx, "(monitor/src/progress.c) ERROR during log_process_data fscanf...");
			logger_send_log(proc_ctx->log_ctx);
			fclose(stat_file);
			continue;
		};

		// Synthesise process data in one line
		sprintf(process_log_data,
				"pid : %d \t"
				"comm : %s \t "
				"state : %c \t "
				"ppid : %d \t "
				"pgrp : %d \t "
				"session : %u \t "
				"utime : %lu \t "
				"stime : %lu \t "
				"num_threads : %ld \t "
				"starttime : %llu \t "
				"vsize : %lu \t "
				"rss : %lu \t "
				"rsslim : %lu \n",
				stat_data.pid, stat_data.comm, stat_data.state, stat_data.ppid, stat_data.pgrp, stat_data.session,
				stat_data.utime, stat_data.stime, stat_data.num_threads, stat_data.starttime, stat_data.vsize,
				stat_data.rss, stat_data.rsslim);

		if (ferror(stat_file))
		{
			// TODO: log error
		}
		if (fclose(stat_file))
		{
			// TODO: log error
		}

		logger_add_log(proc_ctx->log_ctx, process_log_data);
		logger_send_log(proc_ctx->log_ctx);

		// Now deal with his thread(s)
		if ((proc_ctx->is_thread_enabled == true) && (stat_data.num_threads > 1))
		{
			DIR *dir_thread;
			struct dirent *tid;
			sprintf(stat_file_name, "/proc/%s/task", dir->d_name);

			dir_thread = opendir(stat_file_name);
			if (dir_thread == NULL)
			{
				perror("opendir");
				continue;
			}

			while ((tid = readdir(dir_thread)) != NULL)
			{
				// Skip regular files
				if (tid->d_type != DT_DIR)
					continue;

				// Skip non-process (not all digits) directories
				const char *d_name_tid = tid->d_name;
				while (*d_name_tid >= '0' && *d_name_tid <= '9')
				{
					d_name_tid++;
				}
				if (*d_name_tid != '\0')
					continue;

				// Skip PID directory
				if (strcmp(tid->d_name, dir->d_name) == 0)
					continue;

				char stat_thread_file_name[32 + sizeof(stat_file_name) + sizeof(tid->d_name)];
				sprintf(stat_thread_file_name, "%s/%s/stat", stat_file_name, tid->d_name);
				log_thread_data(ctx, process_log_data, stat_thread_file_name);
			}

			if (closedir(dir_thread))
			{
				// TODO: log error
			}
		}
	}
	if (closedir(d))
	{
		// TODO: log error
	}
	TRACE("Processor data saving%s", "\n");
}

static void process_shutdown(void *ctx)
{
	process_ctx *proc_ctx = (process_ctx *)ctx;
	logger_unreg_app(proc_ctx->log_ctx);
	free(proc_ctx);
}

int process_register(el_ctx *el, unsigned poll_interval, bool thread_enabled)
{
	process_ctx *ctx = (process_ctx *)malloc(sizeof(process_ctx));
	ctx->is_thread_enabled = thread_enabled;

	// Initialization of the logger
	logger_ctx_init(&ctx->log_ctx, "PROCESS", "PROCESS monitor");

	periodic_task task = {
		.task = log_process_data,
		.end = process_shutdown,
		.ctx = (void *)ctx,
	};

	return periodic_task_register(el, task, poll_interval);
}

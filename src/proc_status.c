// SPDX-License-Identifier: GPL-2.0-only

#include "proc_status.h"

#include <inttypes.h>
#include <linux/cn_proc.h>
#include <linux/connector.h>
#include <logger/logger.h>
#include <netlink/msg.h>

#define BUF_SIZE 1024

typedef struct
{
	struct nl_sock *sk;
	logger_ctx log_ctx;
} proc_status_ctx;

static void log_proc_status_data(void *ctx, int nlsockfd, unsigned mask)
{
	(void)nlsockfd;

	proc_status_ctx *ps_ctx = (proc_status_ctx *)ctx;
	struct sockaddr_nl nl_addr;
	int nb_bytes_rd;
	unsigned char *rcv_buf;
	char proc_status_data[256];

	if (mask & EL_FINAL)
	{
		goto cleanup;
	}

	nb_bytes_rd = nl_recv(ps_ctx->sk, &nl_addr, &(rcv_buf), NULL);
	if (nb_bytes_rd < 0)
	{
		fprintf(stderr, "Error receiving message: %s\n", nl_geterror(nb_bytes_rd));
		goto cleanup;
	}

	// Loop over received messages
	for (struct nlmsghdr *nlmsghdr = (struct nlmsghdr *)rcv_buf; nlmsg_ok(nlmsghdr, nb_bytes_rd);
		 nlmsghdr = nlmsg_next(nlmsghdr, &nb_bytes_rd))
	{

		// Ignore headers, noop, and anything not PROC
		if ((nlmsghdr->nlmsg_type == NLMSG_ERROR) || (nlmsghdr->nlmsg_type == NLMSG_NOOP))
		{
			continue;
		}
		struct cn_msg *cn_msg = nlmsg_data(nlmsghdr);
		if ((cn_msg->id.idx != CN_IDX_PROC) || (cn_msg->id.val != CN_VAL_PROC))
		{
			continue;
		}

		struct proc_event *proc_ev = (struct proc_event *)cn_msg->data;
		switch (proc_ev->what)
		{
		case PROC_EVENT_NONE:
			sprintf(proc_status_data, "NONE\n");
			break;
		case PROC_EVENT_FORK:
			sprintf(proc_status_data, "FORK: pid=%5u, tgid=%5u, ppid=%5u, ptgid=%5u\n",
					proc_ev->event_data.fork.child_pid, proc_ev->event_data.fork.child_tgid,
					proc_ev->event_data.fork.parent_pid, proc_ev->event_data.fork.parent_tgid);
			break;
		case PROC_EVENT_EXEC:
			sprintf(proc_status_data, "EXEC: pid=%5u, tgid=%5u\n", proc_ev->event_data.exec.process_pid,
					proc_ev->event_data.exec.process_tgid);
			break;
		case PROC_EVENT_UID:
			sprintf(proc_status_data, "UID : pid=%5u, tgid=%5u, ruid=%5" PRIu32 ", euid=%5" PRIu32 "\n",
					proc_ev->event_data.id.process_pid, proc_ev->event_data.id.process_tgid,
					proc_ev->event_data.id.r.ruid, proc_ev->event_data.id.e.euid);
			break;
		case PROC_EVENT_GID:
			sprintf(proc_status_data, "GID : pid=%5u, tgid=%5u, rgid=%5" PRIu32 ", egid=%5" PRIu32 "\n",
					proc_ev->event_data.id.process_pid, proc_ev->event_data.id.process_tgid,
					proc_ev->event_data.id.r.rgid, proc_ev->event_data.id.e.egid);
			break;
		case PROC_EVENT_SID:
			sprintf(proc_status_data, "SID : pid=%5u, tgid=%5u\n", proc_ev->event_data.sid.process_pid,
					proc_ev->event_data.sid.process_tgid);
			break;
		case PROC_EVENT_PTRACE:
			sprintf(proc_status_data, "PTRACE\n");
			break;
		case PROC_EVENT_COMM: {
			// Just making sure we have a trailing '\0',
			// though the specs probably already guarantee this
			char comm[17] = {0};
			memcpy(comm, proc_ev->event_data.comm.comm, 16);
			sprintf(proc_status_data, "COMM: pid=%5u, tgid=%5u, comm=%s\n", proc_ev->event_data.comm.process_pid,
					proc_ev->event_data.comm.process_tgid, comm);
			break;
		}
		case PROC_EVENT_COREDUMP:
			sprintf(proc_status_data, "COREDUMP\n");
			break;
		case PROC_EVENT_EXIT:
			sprintf(proc_status_data,
					"EXIT: pid=%5u, tgid=%5u, ppid=%5u, ptgid=%5u, code=%" PRIu32 ", signal=%" PRIu32 "\n",
					proc_ev->event_data.exit.process_pid, proc_ev->event_data.exit.process_tgid,
					proc_ev->event_data.exit.parent_pid, proc_ev->event_data.exit.parent_tgid,
					proc_ev->event_data.exit.exit_code, proc_ev->event_data.exit.exit_signal);
			break;
		case PROC_EVENT_NONZERO_EXIT:
			sprintf(proc_status_data,
					"NONZERO EXIT: pid=%5u, tgid=%5u, ppid=%5u, ptgid=%5u, code=%" PRIu32 ", signal=%" PRIu32 "\n",
					proc_ev->event_data.exit.process_pid, proc_ev->event_data.exit.process_tgid,
					proc_ev->event_data.exit.parent_pid, proc_ev->event_data.exit.parent_tgid,
					proc_ev->event_data.exit.exit_code, proc_ev->event_data.exit.exit_signal);
			break;
		}
	}

	// "The read data is stored in a newly allocated buffer
	// that is assigned to *buf" - libnl
	// So we need to free the rcv_buf
	free(rcv_buf);

	logger_add_log(ps_ctx->log_ctx, proc_status_data);
	logger_send_log(ps_ctx->log_ctx);

	return;

cleanup:
	logger_unreg_app(ps_ctx->log_ctx);
	nl_socket_free(ps_ctx->sk);
	free(ps_ctx);
	return;
}

int proc_status_register(el_ctx *el)
{
	proc_status_ctx *ps_ctx = (proc_status_ctx *)malloc(sizeof(proc_status_ctx));

	// Initialization of the logger
	logger_ctx_init(&ps_ctx->log_ctx, "PRCS", "Process status");

	// Create sock
	int status;
	struct nl_sock *sk;
	struct nl_msg *nl_frame;

	sk = nl_socket_alloc();
	if (sk == NULL)
	{
		fprintf(stderr, "Failed to alloc netlink socket");
		return EXIT_FAILURE;
	}

	// Register to the Connector's PROC multicast group to
	// be able to receive process events multicasted data
	nl_join_groups(sk, CN_IDX_PROC);

	// Then connect the socket
	status = nl_connect(sk, NETLINK_CONNECTOR);
	if (status < 0)
	{
		fprintf(stderr, "Could not connect to netlink socket : %s\n", nl_geterror(status));
		return EXIT_FAILURE;
	}

	// Creation of the listening request.
	// It follow the Connector NL Protocol defined in
	// connector.h. This Protocol is used for routing
	// message to the right registered entity. In this case
	// the PROC entity.
	// Operations allowed by this entity is define in cn_proc.h

	nl_frame = nlmsg_alloc();
	if (nl_frame == NULL)
	{
		fprintf(stderr, "Failed to alloc netlink message");
		goto error_cleanup_1;
	}

	// First create a simple Netlink message header with the
	// right netlink infos and no payload
	if (nlmsg_put(nl_frame, NL_AUTO_PID, NL_AUTO_SEQ, NLMSG_DONE, 0, 0) == NULL)
	{
		fprintf(stderr, "Could not create netlink message header");
		goto error_cleanup_2;
	}

	// Then append the payload (the Connector Protocol
	// frame)
	// cn_msg.data is a flexible array, it doesn't count
	// when allocating a cn_msg variable. So for safety
	// reason as we need to write in this field, we need to
	// allocate this memory.
	struct cn_msg *cn_frame = malloc(sizeof(struct cn_msg) + sizeof(enum proc_cn_mcast_op));
	if (cn_frame == NULL)
	{
		fprintf(stderr, "Failed to alloc cn message");
		goto error_cleanup_2;
	}

	// Populate the cn_msg
	cn_frame->id.idx = CN_IDX_PROC;
	cn_frame->id.val = CN_VAL_PROC;
	cn_frame->len = sizeof(enum proc_cn_mcast_op);					   // Payload size
	*((enum proc_cn_mcast_op *)cn_frame->data) = PROC_CN_MCAST_LISTEN; // Operation ID

	status = nlmsg_append(nl_frame, &cn_frame, sizeof(struct cn_msg) + sizeof(enum proc_cn_mcast_op), NLMSG_ALIGNTO);
	if (status < 0)
	{
		fprintf(stderr, "Error appending cn message: %s\n", nl_geterror(status));
		goto error_cleanup_3;
	}

	// At the end we have this :
	// +----------+-----+------------+------------------+
	// | nlmsghdr | pad |  cn_msghdr | proc_cn_mcast_op |
	// +----------+-----+------------+------------------+

	// Send the Connector Process Events listen request
	// message to start the listening
	status = nl_send(sk, nl_frame);
	if (status < 0)
	{
		fprintf(stderr, "Error sending message: %s\n", nl_geterror(status));
		goto error_cleanup_3;
	}

	// Message sent, we can free it
	free(cn_frame);
	nlmsg_free(nl_frame);

	// Now prepare to listen to the responses by registering
	// the FD socket in the event loop
	int sk_fd = nl_socket_get_fd(sk);
	if (sk_fd == -1)
	{
		fprintf(stderr, "Error getting the file descriptor\n");
		goto error_cleanup_1;
	}

	// Adding the FD to the event loop with the "something
	// to read" signal (EL_IN)
	if (el_add(el, log_proc_status_data, ps_ctx, sk_fd, EL_IN))
	{
		fprintf(stderr, "Error Adding the NL CN PROC FD to the event loop\n");
		goto error_cleanup_1;
	}

	// Everything is set up, we can now store the context
	ps_ctx->sk = sk;

	return EXIT_SUCCESS;

error_cleanup_3:
	free(cn_frame);
error_cleanup_2:
	nlmsg_free(nl_frame);
error_cleanup_1:
	nl_socket_free(sk);
	free(ps_ctx->log_ctx);
	free(ps_ctx);

	printf("Error registering proc status\n");
	return EXIT_FAILURE;
}
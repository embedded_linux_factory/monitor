// SPDX-License-Identifier: GPL-2.0-only

#include "interrupts.h"
#include "periodic_task.h"
#include "tools.h"

#include <logger/logger.h>
#include <stdint.h>
#include <stdlib.h>

#define LOGS_FILE "/proc/interrupts"

typedef struct
{
	char id[4];
	uint32_t *number;
	char description[100];
} interrupt_data;

typedef struct
{
	uint8_t nb_cpu;
	uint8_t nb_interrupt_lines;
	interrupt_data *data;
	interrupt_data *data_to_send;
	bool first_interrupt;
	logger_ctx log_ctx;
} interrupt_ctx;

static void removeChar(char *str, char c)
{
	int i, j;
	int len = strlen(str);
	for (i = j = 0; i < len; i++)
	{
		if (str[i] != c)
		{
			str[j++] = str[i];
		}
	}
	str[j] = '\0';
}

static void log_interrupt_data(void *ctx)
{
	interrupt_ctx *inte_ctx = (interrupt_ctx *)ctx;

	// Update interrupt data
	FILE *file = NULL;
	// TRACE("[Interrupts] update interrupts data%s", "\n");

	file = fopen(LOGS_FILE, "r");

	if (file == NULL)
	{
		ERROR(true, "[Interrupts] Error when opening LOGS_FILE file");
	}
	else
	{
		char line[300];
		char *index_ptr, *ptr;

		int lineCount = 0;
		ERROR(fgets(line, sizeof(line), file) == NULL, "[Interrupts] Error fgets");

		if (ferror(file))
		{
			ERROR(true, "[Interrupts] Error getting line");
		}

		while (fgets(line, sizeof(line), file) != NULL)
		{

			ptr = strtok(&line[0], " ");
			index_ptr = ptr; // keep the adress we want for the keys
			removeChar(index_ptr, ':');

			if (inte_ctx->first_interrupt)
			{ // if it is the first iteration, it save the id in the data. Id should not change during the run
				strncpy(inte_ctx->data[lineCount].id, index_ptr, sizeof(inte_ctx->data[lineCount].id) - 1);
				strncpy(inte_ctx->data_to_send[lineCount].id, index_ptr, sizeof(inte_ctx->data[lineCount].id) - 1);
			}

			ptr = strtok(NULL, " ");

			int incrementcpu = 0;
			char description[100] = "";

			// use while loop until the line is finish
			while (ptr != NULL)
			{
				if (incrementcpu >= inte_ctx->nb_cpu)
				{
					if (inte_ctx->first_interrupt)
					{
						strncat(description, ptr, sizeof(description) - 1);
						strncat(description, " ", sizeof(description) - 1);
						strncpy(inte_ctx->data_to_send[lineCount].description, description,
								sizeof(inte_ctx->data_to_send[lineCount].description));
						strncpy(inte_ctx->data[lineCount].description, description,
								sizeof(inte_ctx->data[lineCount].description));
					}
				}
				// fill the maps of the current key from the first to the last data_cpus
				else
				{
					inte_ctx->data_to_send[lineCount].number[incrementcpu] =
						atoi(ptr) - inte_ctx->data[lineCount].number[incrementcpu];
					inte_ctx->data[lineCount].number[incrementcpu] = atoi(ptr);
					incrementcpu++;
				}
				ptr = strtok(NULL, " ");
			}
			lineCount++;
		}
		fclose(file); // Don't get the error message to not overwrite the value
	}

	// Log interrupt data
	uint8_t numberOfInterrupt = inte_ctx->nb_interrupt_lines;
	uint8_t numberOfCpu = inte_ctx->nb_cpu;

	// add interrupt for each cpu (for each interrupt)
	char interruptDataStr[10000] = "";
	char *ids_ptr = interruptDataStr;
	size_t ids_size = sizeof(interruptDataStr);
	for (uint8_t i = 0; i < numberOfInterrupt; i++)
	{
		// Skip the CPU if there was no change
		bool cpu_change = false;
		for (uint8_t k = 0; k < numberOfCpu; k++)
		{
			if (inte_ctx->data_to_send[i].number[k] != 0)
			{
				cpu_change = true;
				break;
			}
		}
		if (!cpu_change)
		{
			continue;
		}

		// add id for each interrupt
		// int size = snprintf(ids_ptr, ids_size, "id:%s \t ", inte_ctx->data_to_send[i].id);
		int size = snprintf(ids_ptr, ids_size, "%s ", inte_ctx->data_to_send[i].id);
		if (size < 0)
		{
			continue;
		}
		ids_ptr += size;
		ids_size -= size;

		// add interrupt for each cpu (for each interrupt)
		for (uint8_t k = 0; k < numberOfCpu; k++)
		{
			// int size = snprintf(ids_ptr, ids_size, "cpu%i:%d \t ", k, inte_ctx->data_to_send[i].number[k]);
			int size = snprintf(ids_ptr, ids_size, "%d ", inte_ctx->data_to_send[i].number[k]);
			if (size < 0)
			{
				continue;
			}
			ids_ptr += size;
			ids_size -= size;
		}

		// size = snprintf(ids_ptr, ids_size, "description:%s \n ", inte_ctx->data_to_send[i].description);
		size = snprintf(ids_ptr, ids_size, "%s", inte_ctx->data_to_send[i].description);
		if (size < 0)
		{
			continue;
		}
		ids_ptr += size;
		ids_size -= size;
	}

	logger_add_log(inte_ctx->log_ctx, interruptDataStr);

	inte_ctx->first_interrupt = false;
}

static void interrupts_shutdown(void *ctx)
{
	interrupt_ctx *inte_ctx = (interrupt_ctx *)ctx;

	logger_unreg_app(inte_ctx->log_ctx);

	for (uint8_t i = 0; i < inte_ctx->nb_interrupt_lines; i++)
	{
		free(inte_ctx->data[i].number);
		free(inte_ctx->data_to_send[i].number);
	}
	free(inte_ctx->data);
	free(inte_ctx->data_to_send);
	free(inte_ctx);
}

int interrupts_register(el_ctx *el, unsigned poll_interval)
{
	// Count number of lines in the log file
	// TODO: error handling
	int8_t lines = 0;
	FILE *file = fopen(LOGS_FILE, "r");
	if (file == NULL)
	{
		ERROR(true, "Could not open interrupts file");
	}
	char ch;
	while (!feof(file))
	{
		ch = fgetc(file);
		if (ch == '\n')
		{
			lines++;
		}
	}
	fclose(file);
	lines--;

	// Set up globals
	interrupt_ctx *ctx = (interrupt_ctx *)malloc(sizeof(interrupt_ctx));
	if (ctx == NULL)
	{
		ERROR(true, "Could not allocate interrupt_ctx");
	}
	ctx->nb_cpu = sysconf(_SC_NPROCESSORS_ONLN);
	ctx->nb_interrupt_lines = lines;
	ctx->first_interrupt = true;

	// Initialization of the logger
	logger_ctx_init(&ctx->log_ctx, "INTERRUPTS_LOG", "Interrupts monitor");

	ctx->data = calloc(ctx->nb_interrupt_lines, sizeof(interrupt_data));
	if (ctx->data == NULL)
	{
		ERROR(true, "Could not allocate interrupt_data");
	}

	ctx->data_to_send = calloc(ctx->nb_interrupt_lines, sizeof(interrupt_data));
	if (ctx->data_to_send == NULL)
	{
		ERROR(true, "Could not allocate interrupt_data");
	}

	for (uint8_t i = 0; i < ctx->nb_interrupt_lines; i++)
	{
		ctx->data[i].number = calloc(ctx->nb_cpu, sizeof(uint32_t));
		ctx->data_to_send[i].number = calloc(ctx->nb_cpu, sizeof(uint32_t));
	}

	periodic_task task = {
		.task = log_interrupt_data,
		.end = interrupts_shutdown,
		.ctx = (void *)ctx,
	};
	return periodic_task_register(el, task, poll_interval);
}

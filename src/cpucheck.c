// SPDX-License-Identifier: GPL-2.0-only

#include "cpucheck.h"
#include "periodic_task.h"
#include "tools.h"

#include <logger/logger.h>
#include <stdint.h>
#include <stdlib.h>

#define CPU_AVG_FILE "/proc/loadavg"
#define STAT_FILE	 "/proc/stat"

// CPU working time. Time units are in USER_HZ or
// Jiffies (typically hundredths of a second)
typedef struct
{
	uint32_t user;
	uint32_t nice;
	uint32_t system;
	uint32_t idle;
	uint32_t iowait;
	uint32_t irq;
	uint32_t softirq;
} cpu_working_time;

typedef struct
{
	// Number of jobs in the run queue (state R),
	// or waiting for dirk I/O (state D).
	float loadAvg1min;	// averaged over 1 min.
	float loadAvg5min;	// averaged over 5 min.
	float loadAvg15min; // averaged over 15 min.

	// number of currently executing kernel scheduling entities
	// (processes, threads); this will be less than or equal to the
	// number of CPUs.
	int executingEntities;
	// number of kernel scheduling entities that currently exist on the system.
	int kernelSchedulingEntities;

	// Most recently created processes
	char pidLastProcess[10];

	cpu_working_time allCpuWorkingTime;
	cpu_working_time
		*cpuWorkingTime; /**< number of cpu time spent different kinds of work. CPU0 is cpuWorkingTime[0] ... */
	uint8_t numberCpu;
} cpu_data;

typedef struct
{
	cpu_data *cpu_data;
	logger_ctx log_ctx;
} cpucheck_ctx;

static void log_cpu_data(void *ctx)
{
	cpucheck_ctx *cpu_ctx = (cpucheck_ctx *)ctx;
	cpu_data *cpuData = cpu_ctx->cpu_data;

	int8_t error = 0;
	FILE *fileToRead = NULL;

	// Update load average
	fileToRead = fopen(CPU_AVG_FILE, "r");
	if (fileToRead == NULL)
	{
		ERROR(true, "[CpuCheck] Error when opening CPU_AVG_FILE file");
		error = -1;
	}
	else if (!feof(fileToRead))
	{
		error = fscanf(fileToRead, "%f %f %f %d/%d %s\n", &cpuData->loadAvg1min, &cpuData->loadAvg5min,
					   &cpuData->loadAvg15min, &cpuData->executingEntities, &cpuData->kernelSchedulingEntities,
					   cpuData->pidLastProcess);
		if (error < 0)
		{
			ERROR(true, "[CpuCheck] Error when parsing CPU_AVG_FILE file");
			fclose(fileToRead);
		}
		else
		{
			error = fclose(fileToRead);
		}
	}
	else
	{
		ERROR(true, "[CpuCheck] Log file empty");
	}

	// Update CPU stat
	fileToRead = fopen(STAT_FILE, "r");
	if (fileToRead == NULL)
	{
		ERROR(true, "[CpuCheck] Error when opening STAT_FILE file");
		error = -1;
	}
	else if (!feof(fileToRead))
	{
		error = fscanf(fileToRead, "%*s %u %u %u %u %u %u %u %*[^\n]", &cpuData->allCpuWorkingTime.user,
					   &cpuData->allCpuWorkingTime.nice, &cpuData->allCpuWorkingTime.system,
					   &cpuData->allCpuWorkingTime.idle, &cpuData->allCpuWorkingTime.iowait,
					   &cpuData->allCpuWorkingTime.irq, &cpuData->allCpuWorkingTime.softirq);
		for (size_t i = 0; i < cpuData->numberCpu; i++)
		{
			error = fscanf(fileToRead, "%*s %u %u %u %u %u %u %u %*[^\n]", &cpuData->cpuWorkingTime[i].user,
						   &cpuData->cpuWorkingTime[i].nice, &cpuData->cpuWorkingTime[i].system,
						   &cpuData->cpuWorkingTime[i].idle, &cpuData->cpuWorkingTime[i].iowait,
						   &cpuData->cpuWorkingTime[i].irq, &cpuData->cpuWorkingTime[i].softirq);
		}
		if (error < 0)
		{
			ERROR(true, "[CpuCheck] Error when parsing STAT_FILE file");
			fclose(fileToRead); // Don't get the error message to not overwrite the value
		}
		else
		{
			error = fclose(fileToRead);
		}
	}
	else
	{
		ERROR(true, "[CpuCheck] Log file empty");
	}

	// Log CPU data
	char cpuLoadAvg[1000] = "";
	char strCpuStat[1000] = "";
	char *cpu_stat_ptr = strCpuStat;
	size_t cpu_stat_size = sizeof(strCpuStat);
	snprintf(strCpuStat, sizeof(strCpuStat),
			 "userTimeSpent : %u \t "
			 "nice : %u \t "
			 "system : %u \t "
			 "idle : %u \t "
			 "iowait : %u \t "
			 "irq : %u \t "
			 "softirq : %u \n",
			 cpuData->allCpuWorkingTime.user, cpuData->allCpuWorkingTime.nice, cpuData->allCpuWorkingTime.system,
			 cpuData->allCpuWorkingTime.idle, cpuData->allCpuWorkingTime.iowait, cpuData->allCpuWorkingTime.irq,
			 cpuData->allCpuWorkingTime.softirq);
	for (uint8_t k = 0; k < cpuData->numberCpu; k++)
	{
		int size = snprintf(cpu_stat_ptr, cpu_stat_size,
							"CPU%d \t "
							"userTimeSpent : %u \t "
							"nice : %u \t "
							"system : %u \t "
							"idle : %u \t "
							"iowait : %u \t "
							"irq : %u \t "
							"softirq : %u \n",
							k, cpuData->cpuWorkingTime[k].user, cpuData->cpuWorkingTime[k].nice,
							cpuData->cpuWorkingTime[k].system, cpuData->cpuWorkingTime[k].idle,
							cpuData->cpuWorkingTime[k].iowait, cpuData->cpuWorkingTime[k].irq,
							cpuData->cpuWorkingTime[k].softirq);
		if (size < 0)
		{
			// TODO: do something about those lost logs
			break;
		}
		cpu_stat_ptr += size;
		cpu_stat_size -= size;
	}
	snprintf(cpuLoadAvg, sizeof(cpuLoadAvg),
			 "loadAvg1min :%f \t "
			 "loadAvg5min :%f \t "
			 "loadAvg15min :%f \t "
			 "executingEntities :%d \t "
			 "kernelSchedulingEntities :%d \t "
			 "pidLastProcess :%s \n",
			 cpuData->loadAvg1min, cpuData->loadAvg5min, cpuData->loadAvg15min, cpuData->executingEntities,
			 cpuData->kernelSchedulingEntities, cpuData->pidLastProcess);

	logger_add_log(cpu_ctx->log_ctx, strCpuStat);
	logger_add_log(cpu_ctx->log_ctx, cpuLoadAvg);

	logger_send_log(cpu_ctx->log_ctx);

	ERROR(error < 0, "[CpuCheck] Error when logging processor data");
	// TRACE("Processor data saving%s", "\n");
}

static void cpucheck_shutdown(void *ctx)
{
	cpucheck_ctx *cpu_ctx = (cpucheck_ctx *)ctx;
	logger_unreg_app(cpu_ctx->log_ctx);
	free(cpu_ctx->cpu_data->cpuWorkingTime);
	free(cpu_ctx->cpu_data);
	free(cpu_ctx);
}

int cpu_register(el_ctx *el, unsigned poll_interval)
{
	// Alloc context
	cpucheck_ctx *ctx = (cpucheck_ctx *)malloc(sizeof(cpucheck_ctx));

	ctx->cpu_data = malloc(sizeof(cpu_data));
	if (ctx->cpu_data == NULL)
	{
		free(ctx);
		return -1;
	}

	// Initialization of the logger
	logger_ctx_init(&ctx->log_ctx, "CPU_CHECK", "CPU_CHECK monitor");

	// NB CPUs
	long nb_cpu = sysconf(_SC_NPROCESSORS_ONLN);
	if (nb_cpu == -1)
	{
		free(ctx->cpu_data);
		free(ctx);
		return -1;
	}

	ctx->cpu_data->numberCpu = nb_cpu;

	// CPU working time
	ctx->cpu_data->cpuWorkingTime = calloc(nb_cpu, sizeof(cpu_working_time));
	if (ctx->cpu_data->cpuWorkingTime == NULL)
	{
		free(ctx->cpu_data);
		free(ctx);
		return -1;
	}

	// Periodic task
	periodic_task task = {
		.task = log_cpu_data,
		.end = cpucheck_shutdown,
		.ctx = (void *)ctx,
	};

	if (periodic_task_register(el, task, poll_interval))
	{
		free(ctx->cpu_data->cpuWorkingTime);
		free(ctx->cpu_data);
		free(ctx);
		return -1;
	}

	return 0;
}

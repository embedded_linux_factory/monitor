// SPDX-License-Identifier: GPL-2.0-only

#include "periodic_task.h"

#include <errno.h>
#include <stdint.h>
#include <stdlib.h>
#include <sys/timerfd.h>
#include <unistd.h>

#include <inttypes.h>
#include <stdio.h>

static void on_timer(void *ctx, int timerfd, unsigned mask)
{
#if INTPTR_MAX == INT64_MAX
	// printf("on_timer(%016" PRIiPTR ", %d, %x)\n", (intptr_t)ctx, timerfd, mask);
#else
	// printf("on_timer(%08" PRIiPTR ", %d, %x)\n", (intptr_t)ctx, timerfd, mask);
#endif
	periodic_task *task = (periodic_task *)ctx;

	// End timer upon event loop shut down
	if (mask & EL_FINAL)
	{
		if (task->end != NULL)
		{
			task->end(task->ctx);
		}
		free(task);
		close(timerfd);
		return;
	}

	// Read the timer to see if it's actually our turn
	uint64_t nb_expirations;
	ssize_t nb_read;
	while ((nb_read = read(timerfd, &nb_expirations, 8)) != 8)
	{
		if (errno == EINTR)
		{
			continue;
		}
	}
	if (nb_read == -1 && errno == EAGAIN)
	{
		// Spurious wake up, go back to sleep
		return;
	}
	if (nb_read != 8)
	{
		// TODO: log error
		return;
	}
	if (nb_expirations != 1)
	{
		// TODO: we skipped a beat, log it so we can detect the lag
	}

	// Do the work
	task->task(task->ctx);
}

int periodic_task_register(el_ctx *el, periodic_task user_task, unsigned period)
{
	int timer = timerfd_create(CLOCK_MONOTONIC, TFD_NONBLOCK);
	if (timer == -1)
	{
		return -1;
	}
	struct itimerspec time = {
		.it_interval.tv_sec = period,
		.it_interval.tv_nsec = 0,
		.it_value.tv_sec = 0,
		.it_value.tv_nsec = 1, // Fire ASAP, non-zero to arm the timer
	};
	if (timerfd_settime(timer, 0, &time, NULL))
	{
		return -1;
	}
	periodic_task *task = malloc(sizeof(periodic_task));
	if (task == NULL)
	{
		close(timer);
		return -1;
	}
	*task = user_task;
	if (el_add(el, on_timer, task, timer, EL_IN))
	{
		free(task);
		close(timer);
		return -1;
	}
	return 0;
}

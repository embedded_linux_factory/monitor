// SPDX-License-Identifier: GPL-2.0-only

#include "event_loop.h"

#include <errno.h>
#include <stdlib.h>
#include <sys/epoll.h>
#include <unistd.h>

typedef struct cb_item
{
	el_cb cb;
	void *cb_ctx;
	int fd;
} cb_item;

static uint32_t el_to_epoll(unsigned el)
{
	uint32_t epoll = 0;
	if (el & EL_IN)
		epoll |= EPOLLIN;
	if (el & EL_PRI)
		epoll |= EPOLLPRI;
	if (el & EL_OUT)
		epoll |= EPOLLOUT;
	if (el & EL_ERR)
		epoll |= EPOLLERR;
	if (el & EL_HUP)
		epoll |= EPOLLHUP;
	if (el & EL_RDHUP)
		epoll |= EPOLLRDHUP;
	return epoll;
}

static unsigned epoll_to_el(uint32_t epoll)
{
	unsigned el = 0;
	if (epoll & EPOLLIN)
		el |= EL_IN;
	if (epoll & EPOLLPRI)
		el |= EL_PRI;
	if (epoll & EPOLLOUT)
		el |= EL_OUT;
	if (epoll & EPOLLERR)
		el |= EL_ERR;
	if (epoll & EPOLLHUP)
		el |= EL_HUP;
	if (epoll & EPOLLRDHUP)
		el |= EL_RDHUP;
	return el;
}

const char *el_strerror(int error)
{
	switch (error)
	{
	case EL_OK:
		return "OK";
	case EL_ALLOC:
		return "allocation failed";
	case EL_CREATE:
		return "could not create even loop descriptor";
	case EL_ADD:
		return "could not watch descriptor";
	case EL_DEL:
		return "could not un-watch descriptor";
	case EL_WAIT:
		return "could not wait for next event";
	default:;
	}
	return "Unknown event loop error";
}

int el_init(el_ctx *ctx)
{
	if (ht_init(&ctx->cb_data, 0))
	{
		return EL_ALLOC;
	}
	int epfd = epoll_create1(0);
	if (epfd == -1)
	{
		ht_final(&ctx->cb_data);
		return EL_CREATE;
	}
	ctx->epfd = epfd;
	ctx->timeout_ms = -1; // Wait forever
	ctx->running = false;
	ctx->timed_out = false;
	return EL_OK;
}

int el_final(el_ctx *ctx)
{
	// Notify users that the loop is shutting down
	// Note: we use hash table internals.  Ugly, but I'm not sure
	// writing an iterator API is worth the trouble.
	for (size_t i = 0; i < ctx->cb_data.buf_size; i++)
	{
		// Skip empty entries
		if (ctx->cb_data.keys[i] == -1)
		{
			continue;
		}
		// Call the callback with the shutdown flag
		cb_item *cb = (cb_item *)ctx->cb_data.values[i];
		cb->cb(cb->cb_ctx, cb->fd, EL_FINAL);
		free(cb);
	}

	ht_final(&ctx->cb_data);
	close(ctx->epfd);
	return EL_OK;
}

int el_add(el_ctx *ctx, el_cb cb, void *cb_ctx, int fd, unsigned mask)
{
	cb_item *item = malloc(sizeof(cb_item));
	if (item == NULL)
	{
		return EL_ALLOC;
	}
	item->cb = cb;
	item->cb_ctx = cb_ctx;
	item->fd = fd;
	if (ht_set(&ctx->cb_data, fd, item))
	{
		free(item);
		return EL_ALLOC;
	}

	struct epoll_event event = {
		.events = el_to_epoll(mask),
		.data.ptr = item,
	};
	if (epoll_ctl(ctx->epfd, EPOLL_CTL_ADD, fd, &event) == -1)
	{
		ht_clear(&ctx->cb_data, fd);
		free(item);
		return EL_ADD;
	}

	return EL_OK;
}

int el_del(el_ctx *ctx, int fd)
{
	if (epoll_ctl(ctx->epfd, EPOLL_CTL_DEL, fd, NULL) == -1)
	{
		return EL_DEL;
	}
	free(ht_get(&ctx->cb_data, fd));
	ht_clear(&ctx->cb_data, fd);
	return EL_OK;
}

int el_run(el_ctx *ctx)
{
	ctx->running = true;
	while (ctx->running)
	{
		int error = el_run_once(ctx);
		if (error)
		{
			ctx->running = false;
			return error;
		}
	}
	return EL_OK;
}

int el_run_once(el_ctx *ctx)
{
// Maximum number of events we process at a time.
// I (Loup) have no idea how many is best.  Still more
// than 1 though, to limit the number of system calls.
#define MAX_EVENTS 10

	// Wait for next events (hiding EINTR)
	struct epoll_event events[MAX_EVENTS];
	int nb_fd;
	do
	{
		// The loop might be stopped in a signal handler
		if (!ctx->running)
		{
			return EL_OK;
		}
		nb_fd = epoll_wait(ctx->epfd, events, MAX_EVENTS, ctx->timeout_ms);
	} while (nb_fd == -1 && errno == EINTR);

	// Detect errors & timeout
	if (nb_fd == -1)
	{
		return EL_WAIT;
	}
	ctx->timed_out = nb_fd == 0;

	// Forward each event to its callback
	for (int i = 0; i < nb_fd; i++)
	{
		cb_item *cb = (cb_item *)(events[i].data.ptr);
		unsigned mask = epoll_to_el(events[i].events);
		cb->cb(cb->cb_ctx, cb->fd, mask);
	}

	return EL_OK;
}

void el_stop(el_ctx *ctx)
{
	ctx->running = false;
}

bool el_running(const el_ctx *ctx)
{
	return ctx->running;
}

bool el_timed_out(const el_ctx *ctx)
{
	return ctx->timed_out;
}

void el_set_timeout(el_ctx *ctx, unsigned milliseconds)
{
	ctx->timeout_ms = milliseconds;
}

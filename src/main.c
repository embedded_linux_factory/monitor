// SPDX-License-Identifier: GPL-2.0-only

#include "cpucheck.h"
#include "event_loop.h"
#include "interrupts.h"
#include "proc_status.h"
#include "process.h"
#include "ramcheck.h"

#include <signal.h>
#include <stdio.h>
#include <yaml.h>

#include "logger/logger.h"

typedef struct
{
	yaml_document_t *document;
	yaml_node_t *node;
} y_node;

static y_node y_open(const char *path)
{
	FILE *file = NULL;
	y_node root = {
		.document = malloc(sizeof(yaml_document_t)),
		.node = NULL,
	};
	yaml_parser_t parser;
	bool parser_initialized = false;

	// Check initial allocation
	if (root.document == NULL)
	{
		fprintf(stderr, "Could not allocate yaml_document_t\n");
		goto fail;
	}

	// Open file
	file = fopen(path, "r");
	if (file == NULL)
	{
		perror("Could not open config file");
		goto fail;
	}

	// Initialize parser
	if (!yaml_parser_initialize(&parser))
	{
		fprintf(stderr, "Could not init YAML parser\n");
		goto fail;
	}
	parser_initialized = true;
	yaml_parser_set_input_file(&parser, file);

	// Parse document
	if (!yaml_parser_load(&parser, root.document))
	{
		fprintf(stderr, "Could not parse config file\n");
		yaml_document_delete(root.document); // Clean up partial document
		goto fail;
	}

	// Get root node
	root.node = yaml_document_get_root_node(root.document);
	if (root.node == NULL)
	{
		fprintf(stderr, "Yaml document is empty\n");
		yaml_document_delete(root.document); // Clean up document
		goto fail;
	}

	// Verify root node type
	if (root.node->type != YAML_MAPPING_NODE)
	{
		fprintf(stderr, "Yaml document is not a map.\n");
		yaml_document_delete(root.document); // Clean up document
		goto fail;
	}

	// Clean up temporary resources
	if (parser_initialized)
	{
		yaml_parser_delete(&parser);
	}
	if (file != NULL)
	{
		fclose(file);
	}
	return root;

fail:
	// Clean up all resources in case of failure
	if (root.document != NULL)
	{
		free(root.document);
	}
	root.document = NULL;
	root.node = NULL;

	if (parser_initialized)
	{
		yaml_parser_delete(&parser);
	}
	if (file != NULL)
	{
		fclose(file);
	}
	return root;
}

static void y_close(y_node node)
{
	yaml_document_delete(node.document);
	free(node.document);
}

static y_node y_get_node(y_node n, const char *key)
{
	if (n.node->type != YAML_MAPPING_NODE)
	{
		fprintf(stderr, "Could not find %s: Yaml node is not a map\n", key);
		n.node = NULL;
		return n;
	}
	yaml_node_pair_t *start = n.node->data.mapping.pairs.start;
	yaml_node_pair_t *top = n.node->data.mapping.pairs.top;
	for (yaml_node_pair_t *i = start; i < top; i++)
	{
		yaml_node_t *k = yaml_document_get_node(n.document, i->key);
		yaml_node_t *v = yaml_document_get_node(n.document, i->value);
		if (k->type == YAML_SCALAR_NODE && strcmp((char *)k->data.scalar.value, key) == 0)
		{
			n.node = v;
			return n;
		}
	}
	n.node = NULL;
	return n;
}

static const char *y_get_string(y_node n, const char *key)
{
	y_node node = y_get_node(n, key);
	if (node.node == NULL)
	{
		fprintf(stderr, "Yaml key %s not found\n", key);
		return NULL;
	}
	if (node.node->type != YAML_SCALAR_NODE)
	{
		fprintf(stderr, "Yaml value for %s is not scalar\n", key);
		return NULL;
	}
	return (char *)node.node->data.scalar.value;
}

// TODO: consider using strtoll(3) and reporting errors.
static long y_get_long(y_node n, const char *key)
{
	const char *scalar = y_get_string(n, key);
	if (scalar == NULL)
	{
		return 0;
	}
	return atol(scalar);
}

// TODO: consider reporting errors.
static bool y_get_bool(y_node n, const char *key)
{
	const char *scalar = y_get_string(n, key);
	if (scalar == NULL)
	{
		return false;
	}
	return strcmp(scalar, "true") == 0;
}

static el_ctx event_loop;

void sigint_handler(int signal)
{
	(void)signal;
	el_stop(&event_loop);
}

int main(int argc, char *argv[])
{
	// Handle SIGINT
	struct sigaction signal_data = {
		.sa_handler = sigint_handler,
	};
	if (sigaction(SIGINT, &signal_data, NULL) == -1)
	{
		perror("Could not handle SIGINT");
		return -1;
	}

	if (argc > 2)
	{
		fprintf(stderr, "Usage: %s [path/config.yaml]\n", argv[0]);
	}

	// Read configuration file
	const char *config_file_path = "/etc/ts-monitoring/generic.yaml";
	if (argc == 2)
	{
		config_file_path = argv[1];
	}
	y_node root = y_open(config_file_path);
	if (root.document == NULL)
	{
		return -1;
	}

	bool cpu_enabled = false;
	bool interrupts_enabled = false;
	bool ram_enabled = false;
	bool process_enabled = false;
	bool thread_enabled = false;
	bool procstat_enabled = false;

	unsigned cpu_period = 0;
	unsigned ram_period = 0;
	unsigned interrupts_period = 0;
	unsigned process_period = 0;
	unsigned thread_period = 0;

	y_node cpu_config = y_get_node(root, "cpu");

	if (cpu_config.node == NULL)
	{
		fprintf(stderr, "Failed to read CPU configuration\n");
	}
	else
	{
		cpu_enabled = y_get_bool(cpu_config, "enable");
		cpu_period = y_get_long(cpu_config, "period");
		printf("cpu\n");
		printf("  %s\n", cpu_enabled ? "enabled" : "disabled");
		printf("  check every %u seconds\n", cpu_period);
	}

	y_node interrupts_config = y_get_node(root, "interrupts");

	if (interrupts_config.node == NULL)
	{
	}
	else
	{
		interrupts_enabled = y_get_bool(interrupts_config, "enable");
		interrupts_period = y_get_long(interrupts_config, "period");
		printf("interrupts\n");
		printf("  %s\n", interrupts_enabled ? "enabled" : "disabled");
		printf("  check every %u seconds\n", interrupts_period);
	}

	y_node ram_config = y_get_node(root, "ram");
	if (ram_config.node == NULL)
	{
		fprintf(stderr, "Failed to read ram configuration\n");
	}
	else
	{
		ram_enabled = y_get_bool(ram_config, "enable");
		ram_period = y_get_long(ram_config, "period");
		printf("ram\n");
		printf("  %s\n", ram_enabled ? "enabled" : "disabled");
		printf("  check every %u seconds\n", ram_period);
	}

	y_node process_config = y_get_node(root, "process");

	if (process_config.node == NULL)
	{
		fprintf(stderr, "Failed to read process configuration\n");
	}
	else
	{
		process_enabled = y_get_bool(process_config, "enable");
		process_period = y_get_long(process_config, "period");
		printf("process\n");
		printf("  %s\n", process_enabled ? "enabled" : "disabled");
		printf("  check every %u seconds\n", process_period);
	}

	y_node thread_config = y_get_node(root, "thread");

	if (thread_config.node == NULL)
	{
		fprintf(stderr, "Failed to read process configuration\n");
	}
	else
	{
		thread_enabled = y_get_bool(thread_config, "enable");
		thread_period = process_period;
		printf("thread\n");
		printf("  %s\n", thread_enabled ? "enabled" : "disabled");
		printf("  check every %u seconds as process\n", thread_period);
	}

	y_node procstat_config = y_get_node(root, "procstat");
	if (procstat_config.node == NULL)
	{
		fprintf(stderr, "Failed to read process configuration\n");
	}
	else
	{
		procstat_enabled = y_get_bool(procstat_config, "enable");
		printf("procStat\n");
		printf("  %s\n", procstat_enabled ? "enabled" : "disabled");
	}

	y_close(root);
	printf("Parsing done\n");

	/* Log system configuration */
	logger_reg_app("MONITOR-ELF", "Monitor application");

	// Init event loop
	int error = el_init(&event_loop);
	if (error)
	{
		fprintf(stderr, "Could not start event loop: %s", el_strerror(error));
		if (error != EL_ALLOC)
		{
			perror("System call error");
		}
		return -1;
	}

	// Register all event loop subsystems
	if (cpu_enabled)
	{
		error |= cpu_register(&event_loop, cpu_period);
	}
	if (interrupts_enabled)
	{
		error |= interrupts_register(&event_loop, interrupts_period);
	}
	if (process_enabled)
	{
		error |= process_register(&event_loop, process_period, thread_enabled);
	}
	if (ram_enabled)
	{
		error |= ram_register(&event_loop, ram_period);
	}
	if (procstat_enabled)
	{
		error |= proc_status_register(&event_loop);
	}

	// Run event loop
	if (el_run(&event_loop))
	{
		perror("Event loop unexpectedly stopped");
		return -1;
	}
	printf("Event loop interrupted\n");

	// Clean up event loop
	if (el_final(&event_loop))
	{
		perror("Could not clean up event loop");
		return -1;
	}

	return error;
}

// SPDX-License-Identifier: GPL-2.0-only

#include "ramcheck.h"
#include "common.h"
#include "periodic_task.h"
#include "tools.h"

#include <inttypes.h>
#include <logger/logger.h>
#include <stdlib.h>

#define MEM_INFO_FILE "/proc/meminfo"

/**
 * @brief Structure containing information about memory and processor loads.
 * check https://access.redhat.com/documentation/fr-fr/red_hat_enterprise_linux/6/html/deployment_guide/s2-proc-meminfo
 * for more data informations
 */
typedef struct
{
	uint64_t MemTotal;
	uint64_t MemFree;
	uint64_t Buffers;
	uint64_t MemAvailable;
	uint64_t Cached;
	uint64_t SwapCached;
	uint64_t Active;
	uint64_t Inactive;
	uint64_t Active_anon;
	uint64_t Inactive_anon;
	uint64_t Active_file;
	uint64_t Inactive_file;
	uint64_t Unevictable;
	uint64_t Mlocked;
	uint64_t SwapTotal;
	uint64_t SwapFree;
	uint64_t Dirty;
	uint64_t Writeback;
	uint64_t AnonPages;
	uint64_t Mapped;
	uint64_t Shmem;
	uint64_t Slab;
	uint64_t SReclaimable;
	uint64_t KReclaimable;
	uint64_t SUnreclaim;
	uint64_t KernelStack;
	uint64_t PageTables;
	uint64_t NFS_Unstable;
	uint64_t Bounce;
	uint64_t WritebackTmp;
	uint64_t CommitLimit;
	uint64_t Committed_AS;
	uint64_t VmallocTotal;
	uint64_t VmallocUsed;
	uint64_t VmallocChunk;
	uint64_t Percpu;
	uint64_t HardwareCorrupted;
	uint64_t AnonHugePages;
	uint64_t ShmemHugePages;
	uint64_t ShmemPmdMapped;
	uint64_t FileHugePages;
	uint64_t FilePmdMapped;
	uint64_t CmaTotal;
	uint64_t CmaFree;
	uint64_t HugePages_Total;
	uint64_t HugePages_Free;
	uint64_t HugePages_Rsvd;
	uint64_t HugePages_Surp;
	uint64_t Hugepagesize;
	uint64_t Hugetlb;
	uint64_t DirectMap4k;
	uint64_t DirectMap2M;
} RamData;

typedef struct
{
	logger_ctx log_ctx;
} ramcheck_ctx;

void log_ram_data(void *ctx)
{
	ramcheck_ctx *ram_ctx = (ramcheck_ctx *)ctx;

	// Update RAM data
	RamData ramData;
	int8_t returnError = 0;

	FILE *fileToRead = NULL;
	fileToRead = fopen(MEM_INFO_FILE, "r");

	if (fileToRead == NULL)
	{
		ERROR(true, "[RamCheck] Error when opening MEM_INFO_FILE file");
		returnError = -1;
	}
	else if (!feof(fileToRead))
	{
		returnError = fscanf(fileToRead, "%*s %" SCNu64 " %*[^\n]", &ramData.MemTotal);
		returnError = fscanf(fileToRead, "%*s %" SCNu64 " %*[^\n]", &ramData.MemFree);
		returnError = fscanf(fileToRead, "%*s %" SCNu64 " %*[^\n]", &ramData.MemAvailable);
		returnError = fscanf(fileToRead, "%*s %" SCNu64 " %*[^\n]", &ramData.Buffers);
		returnError = fscanf(fileToRead, "%*s %" SCNu64 " %*[^\n]", &ramData.Cached);
		returnError = fscanf(fileToRead, "%*s %" SCNu64 " %*[^\n]", &ramData.SwapCached);
		returnError = fscanf(fileToRead, "%*s %" SCNu64 " %*[^\n]", &ramData.Active);
		returnError = fscanf(fileToRead, "%*s %" SCNu64 " %*[^\n]", &ramData.Inactive);
		returnError = fscanf(fileToRead, "%*s %" SCNu64 " %*[^\n]", &ramData.Active_anon);
		returnError = fscanf(fileToRead, "%*s %" SCNu64 " %*[^\n]", &ramData.Inactive_anon);
		returnError = fscanf(fileToRead, "%*s %" SCNu64 " %*[^\n]", &ramData.Active_file);
		returnError = fscanf(fileToRead, "%*s %" SCNu64 " %*[^\n]", &ramData.Inactive_file);
		returnError = fscanf(fileToRead, "%*s %" SCNu64 " %*[^\n]", &ramData.Unevictable);
		returnError = fscanf(fileToRead, "%*s %" SCNu64 " %*[^\n]", &ramData.Mlocked);
		returnError = fscanf(fileToRead, "%*s %" SCNu64 " %*[^\n]", &ramData.SwapTotal);
		returnError = fscanf(fileToRead, "%*s %" SCNu64 " %*[^\n]", &ramData.SwapFree);
		returnError = fscanf(fileToRead, "%*s %" SCNu64 " %*[^\n]", &ramData.Dirty);
		returnError = fscanf(fileToRead, "%*s %" SCNu64 " %*[^\n]", &ramData.Writeback);
		returnError = fscanf(fileToRead, "%*s %" SCNu64 " %*[^\n]", &ramData.AnonPages);
		returnError = fscanf(fileToRead, "%*s %" SCNu64 " %*[^\n]", &ramData.Mapped);
		returnError = fscanf(fileToRead, "%*s %" SCNu64 " %*[^\n]", &ramData.Shmem);
		returnError = fscanf(fileToRead, "%*s %" SCNu64 " %*[^\n]", &ramData.KReclaimable);
		returnError = fscanf(fileToRead, "%*s %" SCNu64 " %*[^\n]", &ramData.Slab);
		returnError = fscanf(fileToRead, "%*s %" SCNu64 " %*[^\n]", &ramData.SReclaimable);
		returnError = fscanf(fileToRead, "%*s %" SCNu64 " %*[^\n]", &ramData.SUnreclaim);
		returnError = fscanf(fileToRead, "%*s %" SCNu64 " %*[^\n]", &ramData.KernelStack);
		returnError = fscanf(fileToRead, "%*s %" SCNu64 " %*[^\n]", &ramData.PageTables);
		returnError = fscanf(fileToRead, "%*s %" SCNu64 " %*[^\n]", &ramData.NFS_Unstable);
		returnError = fscanf(fileToRead, "%*s %" SCNu64 " %*[^\n]", &ramData.Bounce);
		returnError = fscanf(fileToRead, "%*s %" SCNu64 " %*[^\n]", &ramData.WritebackTmp);
		returnError = fscanf(fileToRead, "%*s %" SCNu64 " %*[^\n]", &ramData.CommitLimit);
		returnError = fscanf(fileToRead, "%*s %" SCNu64 " %*[^\n]", &ramData.Committed_AS);
		returnError = fscanf(fileToRead, "%*s %" SCNu64 " %*[^\n]", &ramData.VmallocTotal);
		returnError = fscanf(fileToRead, "%*s %" SCNu64 " %*[^\n]", &ramData.VmallocUsed);
		returnError = fscanf(fileToRead, "%*s %" SCNu64 " %*[^\n]", &ramData.VmallocChunk);
		returnError = fscanf(fileToRead, "%*s %" SCNu64 " %*[^\n]", &ramData.Percpu);
		// returnError = fscanf(fileToRead, "%*s %lu %*[^\n]", &ramData.HardwareCorrupted);
		// TODO : it seems that /proc/info on host contains
		// more information than on the target
		// returnError = fscanf(fileToRead, "%*s %lu %*[^\n]", &ramData.AnonHugePages);
		// returnError = fscanf(fileToRead, "%*s %lu %*[^\n]", &ramData.ShmemHugePages);
		// returnError = fscanf(fileToRead, "%*s %lu %*[^\n]", &ramData.ShmemPmdMapped);
		// returnError = fscanf(fileToRead, "%*s %lu %*[^\n]", &ramData.FileHugePages);
		// returnError = fscanf(fileToRead, "%*s %lu %*[^\n]", &ramData.FilePmdMapped);
		// returnError = fscanf(fileToRead, "%*s %lu %*[^\n]", &ramData.CmaTotal);
		// returnError = fscanf(fileToRead, "%*s %lu %*[^\n]", &ramData.CmaFree);
		// returnError = fscanf(fileToRead, "%*s %lu", &ramData.HugePages_Total);
		// returnError = fscanf(fileToRead, "%*s %lu", &ramData.HugePages_Free);
		// returnError = fscanf(fileToRead, "%*s %lu", &ramData.HugePages_Rsvd);
		// returnError = fscanf(fileToRead, "%*s %lu", &ramData.HugePages_Surp);
		// returnError = fscanf(fileToRead, "%*s %lu %*[^\n]", &ramData.Hugepagesize);
		// returnError = fscanf(fileToRead, "%*s %lu %*[^\n]", &ramData.Hugetlb);
		// returnError = fscanf(fileToRead, "%*s %lu %*[^\n]", &ramData.DirectMap4k);
		// returnError = fscanf(fileToRead, "%*s %lu %*[^\n]", &ramData.DirectMap2M);

		if (returnError < 0)
		{
			ERROR(true, "[RamCheck] Error when parsing MEM_INFO_FILE file");
			fclose(fileToRead); // Don't get the error message to not overwrite the value
		}
		else
		{
			returnError = fclose(fileToRead);
		}
	}
	else
	{
		ERROR(true, "[RamCheck] Log file empty");
	}

	// Log RAM data
	char strRamData[1000];
	sprintf(strRamData,
			"MemTotal : %" PRIu64 " \t "
			"MemFree : %" PRIu64 " \t "
			"Buffers : %" PRIu64 " \t "
			"Cached : %" PRIu64 " \t "
			"SwapCached : %" PRIu64 " \t "
			"Active : %" PRIu64 " \t "
			"Inactive : %" PRIu64 " \t "
			"Active_anon : %" PRIu64 " \t "
			"Inactive_anon : %" PRIu64 " \t "
			"Active_file : %" PRIu64 " \t "
			"Inactive_file : %" PRIu64 " \t "
			"Unevictable : %" PRIu64 " \t "
			"Mlocked : %" PRIu64 " \t "
			"SwapTotal : %" PRIu64 " \t "
			"SwapFree : %" PRIu64 " \t "
			"Dirty : %" PRIu64 " \t "
			"Writeback : %" PRIu64 " \t "
			"AnonPages : %" PRIu64 " \t "
			"Mapped : %" PRIu64 " \t "
			"Shmem : %" PRIu64 " \t "
			"Slab : %" PRIu64 " \t "
			"SReclaimable : %" PRIu64 " \t "
			"SUnreclaim : %" PRIu64 " \t "
			"KernelStack : %" PRIu64 " \t "
			"PageTables : %" PRIu64 " \t "
			"NFS_Unstable : %" PRIu64 " \t "
			"Bounce : %" PRIu64 " \t "
			"WritebackTmp : %" PRIu64 " \t "
			"CommitLimit : %" PRIu64 " \t "
			"Committed_AS : %" PRIu64 " \t "
			"VmallocTotal : %" PRIu64 " \t "
			"VmallocUsed : %" PRIu64 " \t "
			"VmallocChunk : %" PRIu64 " \n",
			ramData.MemTotal, ramData.MemFree, ramData.Buffers, ramData.Cached, ramData.SwapCached, ramData.Active,
			ramData.Inactive, ramData.Active_anon, ramData.Inactive_anon, ramData.Active_file, ramData.Inactive_file,
			ramData.Unevictable, ramData.Mlocked, ramData.SwapTotal, ramData.SwapFree, ramData.Dirty, ramData.Writeback,
			ramData.AnonPages, ramData.Mapped, ramData.Shmem, ramData.Slab, ramData.SReclaimable, ramData.SUnreclaim,
			ramData.KernelStack, ramData.PageTables, ramData.NFS_Unstable, ramData.Bounce, ramData.WritebackTmp,
			ramData.CommitLimit, ramData.Committed_AS, ramData.VmallocTotal, ramData.VmallocUsed, ramData.VmallocChunk);

	logger_add_log(ram_ctx->log_ctx, strRamData);
	logger_send_log(ram_ctx->log_ctx);
}

static void ramcheck_shutdown(void *ctx)
{
	ramcheck_ctx *ram_ctx = (ramcheck_ctx *)ctx;
	logger_unreg_app(ram_ctx->log_ctx);
	free(ram_ctx);
}

int ram_register(el_ctx *el, unsigned poll_interval)
{
	ramcheck_ctx *ctx = (ramcheck_ctx *)malloc(sizeof(ramcheck_ctx));

	// Initialization of the logger
	logger_ctx_init(&ctx->log_ctx, "RAM_CHECK", "RAM monitor");

	periodic_task task = {
		.task = log_ram_data,
		.end = ramcheck_shutdown,
		.ctx = (void *)ctx,
	};

	return periodic_task_register(el, task, poll_interval);
}

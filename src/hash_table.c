// SPDX-License-Identifier: GPL-2.0-only

#include "hash_table.h"

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

// Uses Squirrel3 under the hood.
// Note that original Squirrel3 uses 64-bits inputs & outputs.
static size_t hash(int key)
{
	uint64_t idx = (uint64_t)key;
	idx *= 0xB5297A4DB5297A4D;
	idx ^= (idx >> 8);
	idx += 0x68E31DA468E31DA4;
	idx ^= (idx << 8);
	idx *= 0x1B56C4E91B56C4E9;
	idx ^= (idx >> 8);
	return (size_t)idx;
}

static size_t find(ht_ctx *table, int key)
{
	// Normally we'd use a modulo operation, but the buffer size is
	// guaranteed to be a power of 2, so we can just use a mask and
	// avoid the expensive division.
	size_t buf_size_mask = table->buf_size - 1;
	size_t idx = hash(key) & buf_size_mask;
	while (table->keys[idx] != key && table->keys[idx] != -1)
	{
		idx = (idx + 1) & buf_size_mask; // (idx + 1) % table->buf_size
	}
	return idx;
}

int ht_init(ht_ctx *table, size_t size)
{
	// Round up to the next power of 2 (min == 1)
	if (size == 0)
	{
		size = 1;
	}
	size--;
	if (sizeof(size_t) >= 0)
	{
		size |= size >> 1;
	}
	if (sizeof(size_t) >= 0)
	{
		size |= size >> 2;
	}
	if (sizeof(size_t) >= 1)
	{
		size |= size >> 4;
	}
	if (sizeof(size_t) >= 2)
	{
		size |= size >> 8;
	}
	if (sizeof(size_t) >= 4)
	{
		size |= size >> 16;
	}
#if INTPTR_MAX == INT64_MAX
	if (sizeof(size_t) >= 8)
	{
		size |= size >> 32;
	}
#endif
	size++;

	// Alloc initial values
	int *keys = malloc(size * sizeof(int));
	void **values = malloc(size * sizeof(void *));
	if (values == NULL || keys == NULL)
	{
		free(values);
		free(keys);
		return -1;
	}

	// Init structure
	for (size_t i = 0; i < size; i++)
	{
		keys[i] = -1;
	}
	table->keys = keys;
	table->values = values;
	table->nb_items = 0;
	table->buf_size = size;
	return 0;
}

void ht_final(ht_ctx *table)
{
	free(table->values);
	free(table->keys);
	table->keys = NULL;
	table->values = NULL;
	table->nb_items = 0;
	table->buf_size = 0;
}

void *ht_get(ht_ctx *table, int key)
{
	size_t idx = find(table, key);
	return table->keys[idx] == -1 ? NULL : table->values[idx];
}

int ht_set(ht_ctx *table, int key, void *value)
{
	// Insert
	size_t idx = find(table, key);
	int old_slot = table->keys[idx];
	void *old_value = table->values[idx];
	table->keys[idx] = key;
	table->values[idx] = value;

	// Check for occupancy overflow (here we require 25% free slots).
	// Skip resizing if we stayed below.
	if (old_slot == -1)
	{
		table->nb_items++;
	}
	size_t max_size = (table->buf_size >> 1) + (table->buf_size >> 2);
	if (table->nb_items <= max_size)
	{
		return 0;
	}

	ht_ctx new_table;
	if (ht_init(&new_table, table->buf_size << 1))
	{
		// Cancel out the insersion, so failure has no effect
		table->keys[idx] = old_slot;
		table->values[idx] = old_value;
		table->nb_items--;
		return -1;
	}

	// Rehash.  We just doubled the size so the recursion will
	// stay one level deep.  This is slightly less efficient
	// than inserting all over again (by one function call and
	// 2 easily predicted branches), but also much simpler.
	for (size_t i = 0; i < table->buf_size; i++)
	{
		if (table->keys[i] == -1)
		{
			continue;
		}
		ht_set(&new_table, table->keys[i], table->values[i]);
	}

	// Replace the old table by the new.
	ht_final(table);
	*table = new_table;

	return 0;
}

void ht_clear(ht_ctx *table, int key)
{
	size_t idx = find(table, key);

	// Nothing to clear, we're done.
	if (table->keys[idx] == -1)
	{
		return;
	}

	// Clear key.
	table->keys[idx] = -1;
	table->nb_items--;

	// Clear & reinsert the rest of the chain to plug the hole.
	// From https://thenumb.at/Hashtables/#erase-backward-shift
	//
	// It's slower and more complicated than just inserting a tombstone,
	// but it can improve lookup somewhat, and most importantly it means
	// we can stick to one sentinel value (-1 for free slots) instead of
	// two, and simplify the rest of the hash table a bit.
	idx = (idx + 1) & (table->buf_size - 1); // idx++ (modulo buf_size)
	while (table->keys[idx] != -1)
	{
		int key = table->keys[idx];
		void *value = table->values[idx];
		table->keys[idx] = -1;
		table->nb_items--;
		ht_set(table, key, value);
		idx = (idx + 1) & (table->buf_size - 1); // idx++ (modulo buf_size)
	}
}

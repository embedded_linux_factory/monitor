<!-- WARNING : THIS DOCUMENTATION HAS NOT BEEN UPDATED -->



# ELF-MONITOR

### Introduction

The purpose of the software ELF-MONITOR is to improve de visibility for
users of the resource use by an embedded system
(using [ELF][] for example).
This version of ELF-MONITOR logs data from files /proc/load_avg,
/proc/interrupts, /proc/stat, /proc/meminfo and /proc/pid/stat
(with pid a pid of a process).
It logs data to keep track of system information.
This software is useful for gaining visibility into a target's
information.

[ELF]: https://gitlab.com/embedded_linux_factory/elf

### Fonctionality

It allows to collect and log target data.
It can currently collect different types of data:

- Cpu load
- Intrerrupts
- Ram data
- Process data

There are also different ways to log the information:

- **Journalctl.** Data are send with the method `sd_journal_send(3)`.
  It is provided by the systemd api.
  It may be used to submit structured log entries to the system journal.
  It is possible to add variable to the log message.

- **Sysroot.** (soon)

Each data are collected by a unique thread for each data.
So each Tmaj second
(configured to 5 sec for each thread)
a timeout starts collecting data.


```plantuml
@startuml
hide empty description
[*] --> Running : askStartCollector()
Running: entry / updateCollectorData(); logCollectorData();
Running  --> Running : after Tmaj

Running--> [*]
@enduml
```
<!-- #### Load average -->

<!--
The binary "storage_loadavg" when executed create a csv file in the
"outputfiles/loadavg" folder.
The file "data_loadavg.csv" is made of the /proc/loadavg data.
The update time of the /proc/loadavg file is 5sec,
but the acquisition frequency and the number of acquisition is editable,
then the binary get the new data to fill the CSV file.
-->

#### Interrupts

The acquisition is done each 5 seconds (Tmaj).
It will be configurable soon.
For interrupts,
the first acquisition is interrupts since start of the target.
The next acquisitions are the interrupts since the last Tmaj seconds
(Tmaj is the acquisition time).

During the acquisition time,
data are dynamically saved.
At the end of the acquisition,
data are logs with the method previously configured
(journalctl, sysroot, ...).

For journaclt (systemd):
Data are send with the method `sd_journal_send(3)`.
It is provided by the systemd api.
It may be used to submit structured log entries to the system journal.
It is possible to add variable to the log message.

example:
```
sd_journal_send("MESSAGE=interrupts average log savings (see in json format to see more data about interrupts : journalctl -r -o json-pretty)",  // actual human readable message part of the structured message
                "MESSAGE_ID=5dc39e996fd64d0d9acbbeee1db3fd4b",             // 128bit ID that identifies our specific message call, formatted as hexadecimal string. We randomly generated this string with journalctl --new-id128.
                "PRIORITY=5",                                              // numeric message priority valucleare as known from BSD syslog formatted as an integer string
                "NB_CPUS=%li", sysconf(_SC_NPROCESSORS_ONLN),
                "INTERRUPT_DATA=%s", interruptData,
                NULL);
```

The interrupt data collected from /proc/interrups
and contain these information:

- __id:__ id of the interrupt
- __nbrInterruptsCpu0:__ number of interrupts per cpu.
  There can be several number, it depends on the number of cpu.
- __nbrInterruptsCpu1__
- ...
- __nbrInterruptsCpuX__
- __Description:__ description of the interrupt.
  It contains the the interrupt controller and the devices it uses.

These interrupts data will be saved in a new field named INTERRUPT_DATA.
You can see its value by specifying 'json-pretty' in the journalctl
command.

```
journalctl -r -o json-pretty
```

More over you can filter the journalctl logs by MESSAGE_ID.
The message id of the interrupt log is 5dc39e996fd64d0d9acbbeee1db3fd4b.

```
journalctl MESSAGE_ID=52fb62f99e2c49d89cfbf9d6de5e3555
```

The logged data will have the form below:
```
id:0 \t cpu0:31 \t cpu1:0 \t cpu2:0 \t cpu3:0 \t description:IO-APIC  \n id:1 ...
```

#### Cpu

The acquisition is done each 5 seconds.
It will be configurable soon.

During the acquisition time, data are dynamically saved.
At the end of the acquisition, data are logs with the method previously
configured (journalctl, sysroot, ...).

For journaclt (systemd):
Data are send with the method `sd_journal_send(3)`.
It is provided by the systemd api.
It may be used to submit structured log entries to the system journal.
It is possible to add variable to the log message.

example:
```
    sd_journal_send("MESSAGE=Cpu average log savings (see in json format to see more data about processor : journalctl -r -o json-pretty)",  // actual human readable message part of the structured message
                    "MESSAGE_ID=31ec43a6b4c24545bf21791c041c9f89",             // 128bit ID that identifies our specific message call, formatted as hexadecimal string. We randomly generated this string with journalctl --new-id128.
                    "PRIORITY=5",                                              // numeric message priority valucleare as known from BSD syslog formatted as an integer string
                    "NB_CPUS=%li", sysconf(_SC_NPROCESSORS_ONLN),
                    "CPU_LOAD_AVG_DATA=%s", strCpuData,
                    "CPU_STAT_DATA=%s", strCpuStat,
                    NULL);
```

The cpu data collected from /proc/loadavg and contain these information:

- __loadAvg1min:__ brief number of jobs in the run queue (state R) or
  waiting for disk I/O (state D) averaged over 1 min.
- __loadAvg5min:__ brief number of jobs in the run queue (state R) or
  waiting for disk I/O (state D) averaged over 5 min.
- __loadAvg15min:__ brief number of jobs in the run queue (state R) or
  waiting for disk I/O (state D) averaged over 15 min.
- __executingEntities:__ brief number of currently executing kernel
  scheduling entities (processes, threads).
  This will be less than or equal to the number of CPUs.
- __kernelSchedulingEntities:__ brief number of kernel scheduling
  entities that currently exist on the system.

These cpu data will be saved in a new field named CPU_LOAD_AVG_DATA.
You can see its value by specifying 'json-pretty' in the journalctl
command.

```
journalctl -r -o json-pretty
```

Other cpu data are collected from /proc/stat and contain these
information
(These numbers identify the amount of time the CPU has spent performing
different kinds of work):

- __user:__ normal processes executing in user mode.
- __nice:__ niced processes executing in user mode.
- __system:__ processes executing in kernel mode.
- __idle:__ twiddling thumbs.
- __iowait:__ waiting for I/O to complete.
- __irq:__ servicing interrupts.
- __softirq:__ servicing softirqs.

These cpu data will be saved in a new field named `CPU_STAT_DATA`.

More over you can filter the journalctl logs by MESSAGE_ID.
The message id of the cpu log is 31ec43a6b4c24545bf21791c041c9f89.

```
journalctl MESSAGE_ID=31ec43a6b4c24545bf21791c041c9f89
```

The logged data will have the form below:

```
"CPU_LOAD_AVG_DATA" : "loadAvg1min :0.080000 \t loadAvg5min :0.320000 \t loadAvg15min :0.250000 \t executingEntities :3 \t kernelSchedulingEntities :813 \t pidLastProcess :21718 "
"CPU_STAT_DATA" : "userTimeSpent : 213447 \u0009 nice : 943 \u0009 system : 170386 \u0009 idle : 22033760 \u0009 iowait : 24803 \u0009 irq : 0 \u0009 softirq : 10928 \nCPU0 \u0009 userTimeSpent : ... "
```

<!-- #### Sched_debug

The C++ script "storage_sched_debug.cpp" when executed create a csv file
in the "outputfiles/sched_debug" folder.
The file "datas_loadavg.csv" is made of the /proc/sched_debug data.
The acquisition frequency and the number of acquisition is editable,
then the binary get data how many times and at every period defines.
Only the "runnable" part of the file is store.
During the acquisition time, data are stored in unordered_maps.
At the end of the acquisition,
data in the unordered_maps are sent in the CSV file,
stored by task, time and CPU.

Note: if you don't have the feature sched_debug of your system,
exemples are available in the folder "documentations/sched_debug"
-->


#### Ram

The acquisition is done each 5 seconds. It will be configurable soon.

During the acquisition time,
data are dynamically saved.
At the end of the acquisition,
data are logs with the method previously configured
(journalctl, sysroot, ...).

For journaclt (systemd):
Data are send with the method `sd_journal_send(3)`
It is provided by the systemd api.
It may be used to submit structured log entries to the system journal.
It is possible to add variable to the log message.

example:
```
    sd_journal_send("MESSAGE=Ram average log savings (see in json format to see more data about processor : journalctl -r -o json-pretty)",
                    "MESSAGE_ID=31ec43a6b4c24545bf21791c041c9f89",
                    "PRIORITY=5",
                    "NB_CPUS=%li", sysconf(_SC_NPROCESSORS_ONLN),
                    "RAM_DATA=%s", strRamData,
                    NULL);
```

The ram data collected from /proc/meminfo and contain these information
(check
https://access.redhat.com/documentation/fr-fr/red_hat_enterprise_linux/6/html/deployment_guide/s2-proc-meminfo
for more):


- __MemTotal:__ Total amount of usable RAM, in kibibytes,
  which is physical RAM minus a number of reserved bits and the kernel
  binary code.
- __MemFree:__ The amount of physical RAM, in kibibytes,
  left unused by the system.
- __Buffers:__ The amount, in kibibytes,
  of temporary storage for raw disk blocks.
- __Cached:__ The amount of physical RAM, in kibibytes,
  used as cache memory.
- __SwapCached:__ The amount of memory, in kibibytes,
  that has once been moved into swap,
  then back into the main memory,
  but still also remains in the swapfile.
  This saves I/O because the memory does not need to be moved into swap again.
- __Active:__ The amount of memory, in kibibytes,
  that has been used more recently and is usually not reclaimed unless
  absolutely necessary.
- __Inactive:__ The amount of memory, in kibibytes,
  that has been used less recently and is more eligible to be reclaimed
  for other purposes.
- __Active(anon):__ The amount of anonymous and tmpfs/shmem memory,
  in kibibytes,
  that is in active use,
  or was in active use since the last time the system moved something to
  swap.
- __Inactive(anon):__ The amount of anonymous and tmpfs/shmem memory,
  in kibibytes, that is a candidate for eviction.
- __Active(file):__ The amount of file cache memory, in kibibytes,
  that is in active use,
  or was in active use since the last time the system reclaimed memory.
- __Inactive(file):__   The amount of file cache memory, in kibibytes,
  that is newly loaded from the disk, or is a candidate for reclaiming.
- __Unevictable:__ The amount of memory, in kibibytes,
  discovered by the pageout code,
  that is not evictable because it is locked into memory by user
  programs.
- __Mlocked:__ The total amount of memory, in kibibytes,
  that is not evictable because it is locked into memory by user
  programs.
- __SwapTotal:__ The total amount of swap available, in kibibytes.
- __SwapFree:__ The total amount of swap free, in kibibytes.
- __Dirty:__ The total amount of memory, in kibibytes,
  waiting to be written back to the disk.
- __Writeback:__ The total amount of memory, in kibibytes,
  actively being written back to the disk.
- __AnonPages:__ The total amount of memory, in kibibytes,
  used by pages that are not backed by files
  and are mapped into userspace page tables.
- __Mapped:__ The memory, in kibibytes,
  used for files that have been mmaped, such as libraries.
- ...

These ram data will be saved in a new field named RAM_DATA.
You can see its value by specifying 'json-pretty' in the journalctl
command.

```
journalctl -r -o json-pretty
```

More over you can filter the journalctl logs by MESSAGE_ID.
The message id of the ram log is 510ce2bb9c3c407092acbbec567f4593.

```
journalctl MESSAGE_ID=510ce2bb9c3c407092acbbec567f4593
```

The logged data will have the form below:
```
MemTotal : 9922884 \u0009 MemFree : 3247956 \u0009 Buffers : 7623204 \u0009 Cached : 1908496 \u0009 SwapCached : 1781804 \u0009 Active : 0 \u0009 Inactive : 415387 ...
```


#### Process

The acquisition is done each 5 seconds.
It will be configurable soon.

During the acquisition time, data are dynamically saved.
At the end of the acquisition,
data are logs with the method previously configured
(journalctl, sysroot, ...).

For journaclt (systemd):
Data are send with the method `sd_journal_send(3)`.
It is provided by the systemd api.
It may be used to submit structured log entries to the system journal.
It is possible to add variable to the log message.

example:
```
    sd_journal_send("MESSAGE=Process average log savings (see in json format to see more data about processor : journalctl -r -o json-pretty)",
                    "MESSAGE_ID=31ec43a6b4c24545bf21791c041c9f89",
                    "PRIORITY=5",
                    "NB_CPUS=%li", sysconf(_SC_NPROCESSORS_ONLN),
                    "PROCESS_DATA=%s", strProcessData,
                    NULL);
```

The process data collected from /proc/pid/stat (pid of a process)
and contain these information (check `man 5 proc`) for more):

- __pid:__ the process id.
- __comm:__ The filename of the executable, in parentheses.
  This is visible whether or not the executable is swapped out.
- __state:__ process state.
- __ppid:__ pid of the parent of this process.
- __utime:__ Amount of time that this process has been scheduled in user
  mode,
  measured in clock ticks (divide by `sysconf(_SC_CLK_TCK)`).
- __stime:__ Amount of time that this process has been scheduled in
  kernel mode,
  measured in clock ticks (divide by  `sysconf(_SC_CLK_TCK)`).
- ...

These ram data will be saved in a new field named RAM_DATA.
You can see its value by specifying 'json-pretty' in the journalctl
command.

```
journalctl -r -o json-pretty
```

More over you can filter the journalctl logs by MESSAGE_ID.
The message id of the ram log is 35fd4cc7bd5e419d801a090eb084693f.

```
journalctl MESSAGE_ID=35fd4cc7bd5e419d801a090eb084693f
```

The logged data will have the form below:
```
pid : 1 \u0009 comm : (systemd) \u0009  state : S \u0009 ppid : 0 \u0009 pgrp : 1 \u0009 session : 1 \u0009 utime : 72 \u0009 stime : 762 \u0009 num_thread : 1 \u0009 starttime ...
```

### Configuration file

The configuration file allow to enable/disable the data collector.
A default file is present in the repository in config folder
(config/generic.yaml).

When you run the program,
you have to specify the path of the generic config.
It is not necessary but if you don’t specify the path,
it will search the dafault file in /etc/ts-monitoring/generic.yaml.

If the config file is not found,
the program is run with all data collector activated with 5 seconds
timer between acquisition.

#### How to build

For this project we used meson build system(https://mesonbuild.com/).
By default Meson uses the Ninja build system to actually build the code,
so you should install ninja.

To build this project go to the root of the project.
Meson file are already created so no need to initialize the project.

First initialize the builder:

```
meson build
```
A repository build will be created.
Then you can build the project with ninja:

```
ninja -C build
```

After the compilation you could find the executable:
build/src/monitoringExec.
You can change the name of the executable in the meson.build file next
to the main.c file.

Then you can run it:

```
./build/src/monitoringExec
```

#### Yocto recipe
To integrate the project with yocto images
I had to create a monitoring recipe.
I also had to create a recipe to use the libcyaml library.
This recipe is available on open-embedded (recipe libcyaml).

PS: For journalctl
fields larger than 4096 bytes are encoded as null values.
(This may be turned off by passing --all, but be aware that this may
allocate overly long JSON objects.)
https://www.freedesktop.org/software/systemd/man/journalctl.html.


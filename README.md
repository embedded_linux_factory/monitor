# ELF-MONITOR

The ELF Monitor is a program meant to be launched in the background,
to log various events related to processes, memory usage, or CPU use.

[Old README](doc/old_readme.md)


## Dependencies

The monitor depends on various system libraries (systemd,
pthreads…), but most notably libyaml.

The build system used is Meson.

## Setup using ELF
### Install ELF (If not already done)
```sh
$ git clone https://gitlab.com/embedded_linux_factory/elf.git <ELF_PATH>/
```

### Build using ELF
```sh
$ <ELF_PATH>/elfrun build
```

### Run using ELF
```sh
$ <ELF_PATH>/elfrun build/src/monitord config/generic.yaml 
```


## Setup local
### Install the liblogger dependency
```sh
$ git clone https://gitlab.com/embedded_linux_factory/liblogger.git
$ cd liblogger

# Set the `log_system` to `dlt` or `journalctl` depending on the logging system you want to use.
$ meson setup build -Dlog_system=[dlt|journalctl]

$ meson compile -C build/
$ sudo meson install -C build/
$ sudo ldconfig
```

Go back to the monitor directory.
```sh
$ meson setup build 
```


## Compile

```sh
$ meson compile -C build/
```


## Run

```sh
$ build/src/monitord config/generic.yaml
```

## Unit Tests

```sh
$ sudo meson test -C build/
```

